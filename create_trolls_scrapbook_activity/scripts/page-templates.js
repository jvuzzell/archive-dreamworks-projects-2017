//  Copy this code into document.ready of main .js 
//  (don't forget to uncomment it)
//
// $(document).ready(function(){
//	/* PAGE SELECT
//	--------------------------------- */
//	$(".photo-options[data-page]").on("click", function(){
//		PAGE.SELECT(SLIDE.ACTIVE, "page-click", $(this).data("page"));
//	});
	
//	/* PHOTO INPUT HOVER
//	--------------------------------- */
//    $(".inputs a.hover-effect").hover(
//    	function(){
//    		var num = $(this).data("photo");
//        	$("div.photo_"+num).css("background-image","url('img/photo-icon-hover.png')");
//    	}, 
//    	function(){
//        	var num = $(this).data("photo");
//        	$("div.photo_"+num).css("background-image","url('img/photo-icon.png')");
//    	}
//    );
    
//    // ALTER INPUT ON TITLE SELECT SCREEN SO THAT USERS
//    // CAN ACCESS PHOTO ON THE FRONT COVER
//    /* $(".slide[data-slide='3'] input.photo_1").addClass("photo_0").removeClass("photo_1").attr("data-photo","0"); */
    
//     $(".slide[data-slide='3'] a.photo_1").addClass("photo_0").removeClass("photo_1").attr("data-photo","0");
    /* ----------------------------- */
    
//});

var	PAGE = { 
	TOTAL_PHOTOS: 14, 
	PHOTO_INPUT_COUNT: 0, 
	SELECT: function(slide_num, paging_method, num){
		if(paging_method == "nav-button"){
			if(slide_num === 0 || slide_num == 1 || slide_num > 8){return;} 
			if(slide_num == 2 || slide_num == 3){ 
				$(".highlight").addClass("hidden");
				$("#highlight-cover").removeClass("hidden");
				$("#pages, .slide-container").attr("data-page", 0);
				num = 0;
				
			} else if(slide_num >= 4 && slide_num <= 6){
				if(slide_num == 4){num = 1;}
				else if(slide_num == 5){num = 3;}
				else if(slide_num == 6){num = 5;}
				PAGE.EDIT(slide_num, num, "left");
				
			} else if(slide_num == 7){ 
				$("#page-7").removeClass("hidden");
				$(".highlight").addClass("hidden");
				$("#highlight-cover").removeClass("hidden");
				$("#pages, .slide-container").attr("data-page", 7);
				num = 7;
				
			}
			PAGE.REVEAL(num);
		} else if(paging_method == "page-click") { 
			if(
				num == 1 ||
				num == 3 ||
				num == 5
			){ 	
				PAGE.EDIT(slide_num, num, "left");
			} else if(
				num == 2 ||
				num == 4 ||
				num == 6
			){ 	
				PAGE.EDIT(slide_num, num, "right");	
			}
		}
		
	},
	EDIT : function(slideNum, pageNum, pageSide){ 
		var pageTitle;
		$("#pages, .slide-container").attr("data-page", pageNum);
		$(".highlight").addClass("hidden");
		$("#highlight-pg-"+pageSide).removeClass("hidden");
		$(".theme-options[data-page]").css("z-index",20);
		$(".theme-options[data-page='"+pageNum+"']").css("z-index",1000);
		if(pageNum == 1 || pageNum == 2){ 
			pageTitle = "Page 1 & 2";
		} else if(pageNum == 3 || pageNum == 4){ 
			pageTitle = "Page 3 & 4";
		} else if(pageNum == 5 || pageNum == 6){ 
			pageTitle = "Page 5 & 6";
		} 
		$(".slide[data-slide='"+slideNum+"'] .page-title").html(pageTitle);			
	},
	REVEAL : function(pageNum){ 
		$(".page").addClass("hidden").removeClass("active");
		
		switch(pageNum){
			case 0: 
			$("#page-0").removeClass("hidden").addClass("active");
			break;
			case 1: 
			$("#page-1, #page-2").removeClass("hidden").addClass("active");
			break;
			case 2: 
			$("#page-1, #page-2").removeClass("hidden").addClass("active");
			break;
			case 3: 
			$("#page-3, #page-4").removeClass("hidden").addClass("active");
			break; 
			case 4: 
			$("#page-3, #page-4").removeClass("hidden").addClass("active");
			break; 
			case 5: 
			$("#page-5, #page-6").removeClass("hidden").addClass("active");
			break; 
			case 6: 
			$("#page-5, #page-6").removeClass("hidden").addClass("active");
			break; 
			case 7: 
			$("#page-7").removeClass("hidden").addClass("active");
			$("#page-0").addClass("hidden").removeClass("active");
			break;
		}		
	},
	INIT: function(slideNum){ 
		var page_num = "",
			asset_type = "theme",
			page_title = "",
			options = "", 
			numOfInputs = 0;
	
		if(slideNum == 2 || slideNum == 3){
			page_title="Front Cover";
			page_num = 0;
			numOfInputs = 1;
			if(slideNum == 3){asset_type="title";page_title="Title";}
		} 
		else if(slideNum > 3 && slideNum < 7){ 
			page_title="Page ";
			if(slideNum == 4){page_title += slideNum-3;page_num = slideNum-3;}
			if(slideNum == 5){page_title += slideNum-2;page_num = slideNum-2;}
			if(slideNum == 6){page_title += slideNum-1;page_num = slideNum-1;}
		}
		else if(slideNum == 7){
			page_title="Back Cover";
			page_num=slideNum;
			numOfInputs = 1;
		} 
		page_title = "<h1 class='page-title'>"+page_title+"</h1>";
		
		if(slideNum >= 4 && slideNum <= 6){ 
			if(slideNum == 4){numOfInputs = 2;}
			else if(slideNum == 5){numOfInputs = 3;}
			else if(slideNum == 6){numOfInputs = 1;}
			page_title += "<h1 class='page-title page-title-alt hidden'>"+"Page "+(page_num+1)+"</h1>";
			options += PAGE.PHOTO_INPUTS(page_num, numOfInputs, asset_type);
			options += PAGE.THEMES(page_num, asset_type);
			
			if(slideNum == 4){numOfInputs = 1;}
			else if(slideNum == 5){numOfInputs = 2;}
			else if(slideNum == 6){numOfInputs = 3;}
			options += PAGE.PHOTO_INPUTS(page_num+1, numOfInputs, asset_type);
			options += PAGE.THEMES(page_num+1, asset_type);
			
		} else { 
			options += PAGE.PHOTO_INPUTS(page_num, numOfInputs);
			options += PAGE.THEMES(page_num, asset_type);
		}
		
		var htmlString = page_title+options;
		return htmlString;
	}, 
	PHOTO_INPUTS: function(page_num, numOfInputs, asset_type){ 
		var options = "", 
			prevent_this = false;
			
		options += "<div class='photo-options bg-img' data-page='"+page_num+"'>";
		options += "<div class='inputs' data-page='"+page_num+"'>";
		
		for(i = 0; i < numOfInputs; i++){
			if(PAGE.PHOTO_INPUT_COUNT == 2 && prevent_this === false){ 
				PAGE.PHOTO_INPUT_COUNT--;
				prevent_this = true;
			}
						
			options += '<a class="hover-effect photo_'+PAGE.PHOTO_INPUT_COUNT+'" data-photo="'+PAGE.PHOTO_INPUT_COUNT+'"></a>';
		
			PAGE.PHOTO_INPUT_COUNT++; 
		}
		
		options += "</div></div>";
		
		return options;
	},
	THEMES: function(page_num, asset_type){ 
		var options = "";
		
		options += "<div class='theme-options' data-page='"+page_num+"'>";
		
		for(i = 1; i < 6; i++){ 
			options += "<a class='theme-option' data-page='"+page_num+"' data-"+asset_type+"id='"+i+"'><div class='bg-img' data-bgsrc='assets/icon-page-"+page_num+"_"+asset_type+"-"+i+".png'></div><div class='bg-img theme-hover' data-bgsrc='img/option-screens/theme-hover.png'></div></a>";
		}
		
		options += "</div>";
		
		return options;		
	}
};