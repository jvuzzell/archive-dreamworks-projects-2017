# Changelog

## [1.8.2] - 2015-04-13
-Updated poster backgrounds

## [1.8.1] - 2015-04-11
-Minor bug fixes / asset tweaks

## [1.8.0] - 2015-04-08
-Lo-res assets for iOS compatibility
-Removed registered symbol from papercraft assets
-Reptool icons fixed
-Print/share/download button spacing fixed
-Changed builder graphic on welcome screen
-Added timeouts to preloaders to prevent app hang state
-Added retry to asset fetching
-Added asset checking/skipping to PDF build
-Animation improvements

## [1.7.0] - 2015-04-07
-Download popup
-Removed alternate download methods
-Changed build button in final step
-Default character state fixes
-Fixed branding layering on welcome screen
-Null logo on blank nameplate
-Updated logo on poster images
-Updated logo on welcome screen
-Added lo-res version of poster image for AWS and family gallery sharing
-Fixed instruction page crop/scale
-Web PNG optimization to speed up PDF generation
-iOS testing/tweaks

## [1.6.0] - 2015-04-06
-Debug output added to PDF
-Debug function to skip to end of app
-Asset layering changes to prop and friend
-Fixed default character settings
-Fixed papercraft assets not matching web assets
-Updated logo on poster images
-Nudged position of character in poster image
-Papercraft PNG optimization to speed up PDF generation

## [1.5.0] - 2015-04-05
-Implemented intermediary loading animation
-Friend recuts
-iOS/Android fixes/tweaks
-Changed download method to download attribute

## [1.4.0] - 2015-04-04
-Intelligent preloader
-Added cycle button to all name slots
-Added name display at top of naming step
-Tweaked position/size of model in poster image/pdf
-Correct font in poster image/pdf
-Changed download method to 'data:' uri
-Fixed preview image bugs
-Fixed prev/next arrow bugs
-Fixed body conditions not appearing
-PNG optimization / asset consolidation

## [1.3.0] - 2015-04-01
-Option to remove prop and reptool friend
-Responsnive/animation tweaks
-Working loading screen
-Realtime asset swapping

## [1.2.0] - 2015-03-31
-Final step / asset generation reorganiztion
-Fixed share popup being blocked
-Naming steps consolidated/randomized
-Fixed incorrect head/condition alignment in papercraft
-Friend/prop sizing/position

## [1.1.0] - 2015-03-31
-Missing expression icons fixed
-iOS rendering problems fixed
-Responsive tweaks

## [1.0.0] - 2015-03-30
-Initial Build
