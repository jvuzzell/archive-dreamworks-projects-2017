/* Set Delays between slides 
------------------------------- */
function setSlideTransitionDelay(direction){
	if (direction == "next"){ 
		setTimeout(nextSlide, 0);
	} else if (direction == "previous"){ 
		setTimeout(prevSlide, 0);
	}
}

/* SlIDE NAVIGATION 
------------------------------------ */ 
/* NEXT SLIDE */ 
function nextSlide(){ 
	SLIDE.UI_TRANSITION("next");
	PAGE.SELECT(SLIDE.ACTIVE, "nav-button");
	$(".slide-container").attr("data-slide", SLIDE.ACTIVE);
	
	/* UPDATE CURRENT SLIDE APPEARANCE
	-------------------------------------------- */
	if(SLIDE.ACTIVE == 1) {
		setTimeout(function(){ 
			$("#step-backward").css("display","block");
			$("#step-forward, #step-backward").removeClass("hidden");		
		}, 1000);
		delayNavigation("entry",1000);	
	}
	if(SLIDE.ACTIVE == 2){ 
		$(".option-screens").removeClass("hidden").addClass("cover");
		$("div.photo_0b").addClass("flash");
	}
	if(SLIDE.ACTIVE == 3){ 
		SCRAPBOOK.SET_THEME(0, 1, "title"); 
		
		$("#instructional-2, #instructional-1").addClass("hidden");
		$("#instructional-3").removeClass("hidden");
		$("a.theme-option.active div.theme-hover").addClass("flash");
		
	}
	if(SLIDE.ACTIVE == 4){
		$(".option-screens").removeClass("cover").addClass("full-spread");
		$("#instructional-3").addClass("hidden stay-hidden");
		
		$("a.theme-option.active div.theme-hover").removeClass("flash");
		$("div.photo_0b").removeClass("flash");
	
	}
	if(SLIDE.ACTIVE == 7){ 
		$(".option-screens").removeClass("full-spread").addClass("cover");
	}
	if(SLIDE.ACTIVE == 8){ 
		$(".option-screens").removeClass("cover").addClass("hidden");
		$stepForward.addClass("force-exit");
		
	}
}

/* PREVIOUS SLIDE */ 
function prevSlide(){ 
	SLIDE.UI_TRANSITION("previous");
	PAGE.SELECT(SLIDE.ACTIVE, "nav-button");
	$(".slide-container").attr("data-slide", SLIDE.ACTIVE);
	
	/* UPDATE CURRENT SLIDE APPEARANCE
	-------------------------------------------- */
	if(SLIDE.ACTIVE === 0){ 
		$stepForward.addClass("hidden");
		$stepBackward.addClass("hidden");
		$("#step-backward").css("display","none");
	}
	if(SLIDE.ACTIVE == 1){ 
		$(".option-screens").addClass("hidden");
	}
	if(SLIDE.ACTIVE == 2){ 
		$("#instructional-3").addClass("hidden");
		$("#instructional-1, #instructional-2").removeClass("hidden");
	}
	if(SLIDE.ACTIVE == 3){ 
		$(".option-screens").removeClass("full-spread").addClass("cover");
		$("#instructional-3").removeClass("hidden stay-hidden");
	
	}
	if(SLIDE.ACTIVE == 6){
		$(".option-screens").removeClass("cover").addClass("full-spread");
	}
	if(SLIDE.ACTIVE == 7){ 
		$(".option-screens").removeClass("hidden").addClass("cover");
		$stepForward.removeClass("force-exit");
	}
}

/* SLIDE TRANSITION UTILITIES */
/* NOTE: these functions are very specific elements 
               and may not translate to other projects */ 
function returnToStart(){ 
	delayNavigation("exit",0);
	SCRAPBOOK.PDF = new jsPDF('p', 'in', [8.5, 11]);
	$("#step-forward").removeClass("force-exit");
	$(".slide[data-slide='8']").removeClass("active-slide").addClass("hidden");	
	$("#instructional-1").removeClass("hidden");
	setTimeout(function(){
		SLIDE.JUMPBACKWARD(0);
	}, 800);
	$(".theme-options .theme-option").removeClass("active");
	$(".theme-options .theme-option[data-themeid='1']").addClass("active");
	$(".theme-options .theme-option[data-titleid='1']").addClass("active");

	for(i=0;i < 8; i++){ 
		var page = SCRAPBOOK["PAGES"]["page_"+i];
		if(i == 0){ 
			page.title = undefined;
		} 
		page.theme = 1;
		page.photoIDs = [];
		SCRAPBOOK.PHOTOS = [];
		SCRAPBOOK.LOAD_PAGE(i);
	} 
}

function delayNavigation(direction, delay){ 
	setTimeout(function(){ 
		if(direction == "entry"){ 
			$("#step-forward, #step-backward").addClass("enter").removeClass("exit");
		} else if(direction == "exit"){ 
			$("#step-forward, #step-backward").removeClass("enter").addClass("exit");
		}
	}, delay);
}

function leaveCurrentSlide(){ 
	var $currentSlide = $('div.active-slide[data-slide='+ SLIDE.ACTIVE +']'); 
	$currentSlide.addClass("exit");
}

function resetPosterPreviewPosition(){ 
	$(".poster-preview").removeClass("reveal").addClass("exit");
	setTimeout(function(){
		$("poster-preview").removeClass("exit");
	}, 1500);
}

function loadingScreenExit(){ 
	$("#assets-loaded, #loading-text").addClass("exit");
	$("#assets-loaded").css("overflow", "visible");
	$("#assets-not-loaded").css("display", "none");
	setTimeout(
		function(){$("#assets-loaded, #assets-not-loaded, #loading-text").addClass("hidden");

		setTimeout(function(){ 
			// Show the first slide! 
			$(".make-volume, #first-screen").removeClass("hidden");
			$("#first-screen").addClass("active-slide");
		}, 250);
	}, 2000); 
}

function disableNav(){ 
	$("#step-forward > a.hit-box, #step-backward > a.hit-box").css("pointer-events", "none");
}

function enableNav(){ 
	setTimeout(function(){ 
		$("#step-forward > a.hit-box, #step-backward > a.hit-box").css("pointer-events", "auto");
	}, 250);
}