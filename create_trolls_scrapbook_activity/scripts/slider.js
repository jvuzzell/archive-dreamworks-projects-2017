var	SLIDE = { 
	COUNT: 0, 
	ACTIVE: 0, 
	INACTIVE: 0,
	GOTO: 0,
	MOVE: function(direction){
		SLIDE.INACTIVE = SLIDE.ACTIVE;

	 	if(direction == "next"){ 
		 	SLIDE.GOTO = SLIDE.INACTIVE + 1;
		 	if(SLIDE.GOTO > SLIDE.COUNT){ 
		 		SLIDE.GOTO = SLIDE.COUNT; 
		 	}  
	 	} else if(direction == "previous") { 
		 	SLIDE.GOTO = SLIDE.INACTIVE - 1;
		 	if(SLIDE.GOTO < 0){ 
			 	SLIDE.GOTO = 0;
		 	}
	 	}		

		SLIDE.ACTIVE = SLIDE.GOTO;
		
		return null;
	}, 
	LENGTH: function(){ 
		SLIDE.COUNT = $('.slide-container > .slide').length - 1;
		return null;
	}, 
	JUMPFORWARD: function(slideNum){ 
		SLIDE.ACTIVE = slideNum;
		SLIDE.GOTO = slideNum;
		
		jumpToSlide = true;
		nextSlide();
	},
	JUMPBACKWARD: function(slideNum){ 
		SLIDE.ACTIVE = slideNum;
		SLIDE.GOTO = slideNum;
		
		jumpToSlide = true;
		prevSlide();	
	},
	UI_TRANSITION: function(direction){
		if(jumpToSlide === false) {
			if(direction == "next"){ 
				SLIDE.MOVE("next");
			} else if (direction == direction){ 
				SLIDE.MOVE("previous");
			}
		} else { 
			jumpToSlide = false;
		}
		
		var $currentSlide = $('div.active-slide[data-slide='+ SLIDE.INACTIVE +']');
		var $nextSlide = $('div.hidden[data-slide='+ SLIDE.GOTO +']');

		if($nextSlide.length > 0){ 
			// Update slide in user interface
			$currentSlide.removeClass("active-slide exit").addClass("hidden");
			$nextSlide.removeClass("hidden").addClass("active-slide");
		}
	},
	INIT: function(){ 
		var $slide = $("div[data-slide]"); 
	
		$.each($slide, function(index, elem){
			var slideNum = $(elem).data("slide");			
			if(slideNum > 1 && slideNum < 8){$('div[data-slide="'+slideNum+'"]').html(PAGE.INIT(slideNum));}	
		});
	},
	VISITED: [], 
	INSTRUCTIONS : { 
		"FIRSTTIME" : true, 
		"PIC_UPLOADED" : false, 
		"NO_INSTRUCT" : false
	}
};