var assets = {};
function registerAsset() { if(!assets[this.src]) assets[this.src] = this;}

var PATH = document.location.href;
var LOADED = false;

$(window).load(initPreload);

function initPreload() {

  var preloadTotal = 0;
  var preloadCount = 0;
  var positiveCountValue = 0;
  //Start app after 20 seconds regardless of asset availability
  setTimeout(startApp, 20000);  

  function checkLoadedAndInit() {
    
    registerAsset.call(this);
    
    preloadCount--;
    positiveCountValue++;

    // Left to Right loading animation
    // $("#assets-loaded").css("height", (100 * parseFloat(preloadCount / preloadTotal)) + "%");

    // Down to Up loading animation
    $("#assets-loaded").css("height", (118 * parseFloat(positiveCountValue / preloadTotal)) + "%");
    
    //sometimes we don't make it to zero here so give a margin of error
    //img.onload may not be reliable
    if(preloadCount < 5) { 
      setTimeout(startApp, 2000);
    }
  }

	//now that we've kicked off our preloader as early as possible,
	//set proper src's on images we've purposefully deferred on

	$("img[data-src]").each(function(){
    var src = $(this).data('src');
    if(!(src in assets)) {
      var img = new Image();
      img.onload = checkLoadedAndInit;
      img.onerror = preloadFailed;
      img.src = src;
      preloadTotal++;
      preloadCount++;
      assets[img.src] = null;
      $(this).attr('src', src);
    }
  });

  $("[data-bgsrc]").each(function(){
    var src = $(this).data('bgsrc');
    if(!(src in assets)) {
      var img = new Image();
      img.onload = checkLoadedAndInit;
      img.onerror = preloadFailed;
      img.src = src;
      preloadTotal++;
      preloadCount++;
      assets[img.src] = null;
      $(this).css('background-image', 'url(' + src + ')');
    }
  });

  //audio preload
  sfx_music = new Audio;
  sfx_music.onload = checkLoadedAndInit;
  sfx_music.onerror = audioPreloadFailed;
  sfx_music.src = 'assets/sound/music.mp3';

  sfx_nav_click = new Audio;
  sfx_nav_click.onload = checkLoadedAndInit;
  sfx_nav_click.onerror = audioPreloadFailed;
  sfx_nav_click.src = 'assets/sound/option-click.mp3';

  sfx_opt_click = new Audio;
  sfx_opt_click.onload = checkLoadedAndInit;
  sfx_opt_click.onerror = audioPreloadFailed;
  sfx_opt_click.src = 'assets/sound/option-click.mp3';

  preloadTotal+=2;
  preloadCount+=2;
  
  // Set volume for sound effects so that
  // they do not "override" each other.
  audioEqualizer();
}

function preloadFailed() {
  //simply try again here, and then give up

  var img = new Image();
  img.onload = this.onload;

  //be careful uncommenting this line- it will repeatedly try to fetch failed images
  //this will be an infinite loop for most errors (404 are the most obvious)
  //img.onerror = this.onerror;

  img.src = this.src;
}

function audioPreloadFailed() {
  var sfx = new Audio();
  sfx.onload = this.onload;
  sfx.src = this.src;
}