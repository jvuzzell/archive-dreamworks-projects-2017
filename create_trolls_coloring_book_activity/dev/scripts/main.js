var RESTART = false;
var jumpToSlide = false;
var sfx_music, sfx_scream, sfx_click, mute = false, firstplay = true;
var SLIDE = { 
	COUNT: 0, 
	ACTIVE: 0, 
	INACTIVE: 0,
	GOTO: 0,
	MOVE: function(direction){
		SLIDE.INACTIVE = SLIDE.ACTIVE;

	 	if(direction == "next"){ 
		 	SLIDE.GOTO = SLIDE.INACTIVE + 1;
		 	if(SLIDE.GOTO > SLIDE.COUNT){ 
		 		SLIDE.GOTO = SLIDE.COUNT; 
		 	}  
	 	} else if(direction == "previous") { 
		 	SLIDE.GOTO = SLIDE.INACTIVE - 1;
		 	if(SLIDE.GOTO < 0){ 
			 	SLIDE.GOTO = 0;
		 	}
	 	}		

		SLIDE.ACTIVE = SLIDE.GOTO;
		
		return null;
	}, 
	LENGTH: function(){ 
		SLIDE.COUNT = $('.slide-container > .slide').length - 1;
		return null;
	}, 
	JUMPFORWARD: function(slideNum){ 
		SLIDE.ACTIVE = slideNum;
		SLIDE.GOTO = slideNum;
		
		jumpToSlide = true;
		nextSlide();
	},
	JUMPBACKWARD: function(slideNum){ 
		SLIDE.ACTIVE = slideNum;
		SLIDE.GOTO = slideNum;
		
		jumpToSlide = true;
		prevSlide();	
	},
	UI_TRANSITION: function(direction){
		if(jumpToSlide == false) {
			if(direction == "next"){ 
				SLIDE.MOVE("next");
			} else if (direction == direction){ 
				SLIDE.MOVE("previous");
			}
		} else { 
			jumpToSlide = false;
		}
		
		var $currentSlide = $('div.active-slide[data-slide='+ SLIDE.INACTIVE +']');
		var $nextSlide = $('div.hidden[data-slide='+ SLIDE.GOTO +']');

		if($nextSlide.length > 0){ 
			// Update slide in user interface
			$currentSlide.removeClass("active-slide exit").addClass("hidden");
			$nextSlide.removeClass("hidden").addClass("active-slide");
		}
	},
	VISITED: []
}

$(document).ready(function(){
	FastClick.attach(document.body);
	
	/* SET STAGE DIMENSIONS
	---------------------------------- */
	bindWindowSizeEvent(function(){
		$("#stage, #option-screens-bg, .option-screens").width("100%");
		$("#stage, #option-screens-bg, .option-screens").height((9 * parseInt($("#stage").width())) / 16);
	});

	resetVisitedSlides();

	$(".pod-asset, .name-asset, #pod-acc").addClass("hidden");
	
	/* STEPPING THROUGH SLIDES
	---------------------------- */
	SLIDE.LENGTH();

	$startButton = $("#start-button-flower");
	$stepForward = $("#step-forward"); 
	$stepBackward = $("#step-backward"); 

	$startButton.on("click", function(){
		leaveCurrentSlide();
		setSlideTransitionDelay("next");
	});

	$("#step-forward > a.hit-box").on("click", function(){
		leaveCurrentSlide();
		setSlideTransitionDelay("next");
	});

	$("#step-backward > a.hit-box").on("click", function(){
		leaveCurrentSlide();
		setSlideTransitionDelay("previous");
	});

	$("#step-forward > a.hit-box, #step-backward > a.hit-box").hover(function(){
		$(this).parent().addClass('hover');
	}, function(){
		$(this).parent().removeClass('hover');
	});
	/*---------------------------*/
	
	$(window).on("blur", pauseAudio);
	$(window).on("focus", playAudio);
});

/* START APP!!!
----------------------------------- */

function startApp() {
	if(!LOADED) {
		
		loadingScreenExit();
		
		auxillaryAssetLoading();
		Set1CharacterAssetLoading();
		//iOS hack fix
		try {
			if(
			/iPad|iPhone|iPod/i.test(navigator.userAgent) && 
			!window.MSStream 
		
			// &&
			// !/os 9_/i.test(navigator.userAgent) && 
			// !/os 10_/i.test(navigator.userAgent) && 
			// !/os 11_/i.test(navigator.userAgent) && 
			// !/os 12_/i.test(navigator.userAgent)
		
			) {
				setTimeout(Set2CharacterAssetLoading, 5000);
				setTimeout(podRelatedAssetLoading, 10000);
				setTimeout(nameRelatedAssetLoading, 17000);
			} else { 
				setTimeout(Set2CharacterAssetLoading, 1000);
				setTimeout(podRelatedAssetLoading, 5000);
				setTimeout(nameRelatedAssetLoading, 10000);
				
			}
		} catch(e) {}

		// Slide transition after gender selected 

		// FOR WORKING ON SPECIFIC PAGES ///////////////////
		// initGENDER(2);
		// SLIDE.JUMP(13); 
		// $(".name-asset").removeClass("hidden");
		////////////////////////////////////////////////

		//enable audio
		$('#play').click(playAudio);
		$('#pause').click(function(){if(firstplay == true){firstplay = false; playAudio();}else{pauseAudio()}});
		$("#start-button").click(function(){
			firstplay = false;
			if(!mute) playAudio();
			// if(!mute) sfx_nav_click.play();
		});
		$('a.option, a, #refresh, #name-1, #name-2, #name-3, .forward, .backward').click(function(){
			if(!mute) sfx_opt_click.play();
		});
		$('#step-forward, #step-back').click(function(){
			if(!mute) sfx_nav_click.play();
		});
			
		LOADED = true;
	}
}

function playAudio() {
	$('#play').css('display','none');
	$('#pause').css('display','block');
	mute = false;
	if(firstplay) return;
	
	sfx_music.play();
	sfx_music.loop = true;
}

function pauseAudio() {
	$('#pause').css('display','none');
	$('#play').css('display','block');
	mute = true;
	sfx_music.pause();
	sfx_nav_click.pause();
	sfx_nav_click.currentTime = 0;
	sfx_opt_click.pause();
	sfx_opt_click.currentTime = 0;
}

// Called in Preload.js
function audioEqualizer(){ 
	sfx_music.volume     = 1.0;
	sfx_opt_click.volume = 0.4;
	sfx_nav_click.volume = 0.35;
}

/* Set Delays between slides 
------------------------------- */
function setSlideTransitionDelay(direction){
	// NOTE: SLIDE.ACTIVE is the slide that you are leaving
	if(SLIDE.ACTIVE == 12){ 
		disableNav();
	}
	if(SLIDE.ACTIVE == 1){ 
		delayNavigation("exit",0);

	} else if(SLIDE.ACTIVE == 2){ 
		if(direction == "previous"){
			delayNavigation("exit",0);
			trollGenderSlideExit(direction);
		} else { 
			trollGenderSlideExit(direction);
		}

	} else if(SLIDE.ACTIVE == 3){ 
		if(direction == "previous"){ 
			delayNavigation("exit",0);
			assetZoneExit();
			optionSlideExit();
		}
	}

	if(SLIDE.ACTIVE > 3 && SLIDE.ACTIVE < 13){
		if(SLIDE.ACTIVE == 9){ 
			if (direction == "next"){ 
				assetZoneExit();
				setTimeout(nextSlide, 1500);
			} else if(direction == "previous"){ 
				prevSlide();
			}
		} else if(SLIDE.ACTIVE == 10){ 
			if (direction == "next"){ 
				nextSlide();
			} else if(direction == "previous"){ 
				assetZoneExit();
				setTimeout(prevSlide, 1480); 
			}		
		} else { 
			//default
			if (direction == "next"){ 
				nextSlide();
			} else if(direction == "previous"){ 
				prevSlide();
			}

		}
	} else {
		if(SLIDE.ACTIVE == 2){ 
			if (direction == "next"){ 
				setTimeout(nextSlide, 1950);
			} else if (direction == "previous"){ 
				setTimeout(prevSlide, 1950);
			}

		} else if(SLIDE.ACTIVE == 3){ 
			if(direction == "next"){ 
				nextSlide();
			} else { 
				setTimeout(prevSlide, 1480);
			}
		} else if(SLIDE.ACTIVE == 13){
			if(direction == "next"){
				setTimeout(nextSlide, 1500);
				optionSlideExit();
				delayNavigation("exit",0);
			} else if(direction == "previous"){ 
				prevSlide();
			}
		} else if(SLIDE.ACTIVE == 14){
			if(direction == "previous"){ 
				resetPosterPreviewPosition();
				setTimeout(prevSlide, 1480);
				delayNavigation("exit",0);
			} else if(direction == "next"){ 
				setTimeout(nextSlide, 1480);
			}
		} else { 
			// default
			if (direction == "next"){ 
				setTimeout(nextSlide, 1480);
			} else if (direction == "previous"){ 
				setTimeout(prevSlide, 1480);
			}
		}
	}	
}


/* SlIDE NAVIGATION 
------------------------------------ */ 
/* NEXT SLIDE */ 
function nextSlide(){ 
	enableNav();

	SLIDE.UI_TRANSITION("next");

	/* UPDATE CURRENT SLIDE APPEARANCE
	-------------------------------------------- */
	if(SLIDE.ACTIVE == 1) {
		setTimeout(function(){ 
			$("#step-forward, #step-backward").removeClass("hidden");	
		}, 3600);
		delayNavigation("entry",3600);	
	}
	if(SLIDE.ACTIVE == 2){ 
		setTimeout(function(){
			$("#growing-speech-bubble .center").removeClass("collapsed").addClass("expanded");
		}, 2600);
		if(!$("#girl").hasClass("selected") && !$("#boy").hasClass("selected")){
			// delayNavigation("entry",5450);
		} else { 
			delayNavigation("entry",3650);
		}
		highlightSelectedGender();
	}
	if(SLIDE.ACTIVE == 3){ 	
		setTimeout(function(){ 
			$("#step-backward").css("display","block").removeClass("force-exit");
			delayNavigation("entry",0);
		}, 1250);

		initGENDER(PARTNUM.d);
		if(DISPLAYSELECTED == false){
			$("#expression, #hair-acc, #outfit-front, #outfit-back, #attire-acc").addClass("hidden");
		}
		$("#option-screens-bg").fadeIn();
		$(".option-screens").removeClass("hidden");
		$("#asset-display-zone").css("display", "block");
		$("#growing-speech-bubble .center").removeClass("expanded").addClass("collapsed");

		removeSelectedGenderHighlight();
	}
	if(SLIDE.ACTIVE == 4){assetReveal(4);}
	if(SLIDE.ACTIVE == 5){assetReveal(5);}
	if(SLIDE.ACTIVE == 6){assetReveal(6);}
	if(SLIDE.ACTIVE == 7){assetReveal(7);}
	if(SLIDE.ACTIVE == 8){assetReveal(8);}
	if(SLIDE.ACTIVE == 9){assetReveal(9);}
	if(SLIDE.ACTIVE == 10){assetReveal(10);}
	if(SLIDE.ACTIVE == 11){assetReveal(11);}

	if(SLIDE.ACTIVE == 7 || SLIDE.ACTIVE == 9){ 
		$("#sidebar-decorations").addClass("hidden");
		$("#option-none").removeClass("hidden");
	}
	if(SLIDE.ACTIVE == 8 || SLIDE.ACTIVE == 10){ 
		$("#sidebar-decorations").removeClass("hidden");
		$("#option-none").addClass("hidden");
	}
	if(SLIDE.ACTIVE == 10){ 
		$("#troll-shadow").fadeOut();
		$("#pod-shadow").fadeIn();
		$("#asset-display-zone").removeClass("position-1").addClass("position-2");
		$("#pod-leaves, #pod, .vines").removeClass("hidden");
		// Add any custom positioning to troll
		removeTrollPositioning("solo-troll");
		customTrollPositioning("pod-troll");

	}
	if(SLIDE.ACTIVE == 12){$(".name-asset").removeClass("hidden");}
	if(SLIDE.ACTIVE == 13){ 
		$stepForward.addClass("force-exit");
		setTimeout(function(){$stepForward.addClass("hidden");}, 400);
	}

	if(SLIDE.ACTIVE == 14){ 
		customTrollPositioning("preview-poster");
		$stepForward.removeClass("force-exit");
		$("#option-screens-bg").fadeOut();
		$(".option-screens").removeClass("visible").addClass("hidden");
		$("#asset-display-zone").css("display", "none");
		delayNavigation("entry",2500);
		
	}
}

/* PREVIOUS SLIDE */ 
function prevSlide(){ 
	enableNav();

	SLIDE.UI_TRANSITION("previous");

	/* UPDATE CURRENT SLIDE APPEARANCE
	------------------------------------- */
	if(SLIDE.ACTIVE < 14 && SLIDE.ACTIVE > 3){ 
		$("#option-screens-bg").fadeIn();	
	}
	if(SLIDE.ACTIVE < 3){ 
		$("#option-screens-bg").fadeOut();
		$(".option-screens").removeClass("visible").addClass("hidden");
		$("#asset-display-zone").css("display", "none");
	}
	if(SLIDE.ACTIVE < 14 && SLIDE.ACTIVE > 2){ 
		$(".option-screens").removeClass("hidden").addClass("visible");
	}
	if(SLIDE.ACTIVE == 0){ 
		$stepForward.addClass("hidden");
		$stepBackward.addClass("hidden");
	}
	if(SLIDE.ACTIVE == 1){ 

		$("#growing-speech-bubble .center").addClass("collapsed").removeClass("expanded"); 
		delayNavigation("entry",3600);		
		removeSelectedGenderHighlight();
	}
	if(SLIDE.ACTIVE == 2){ 
		$("#step-backward").css("display","none");
		setTimeout(function(){
			$("#growing-speech-bubble .center").removeClass("collapsed").addClass("expanded");
		}, 2600);
		delayNavigation("entry",2900);		
		highlightSelectedGender();
	}
	if(SLIDE.ACTIVE == 6 || SLIDE.ACTIVE == 8){ 
		$("#sidebar-decorations").removeClass("hidden");
		$("#option-none").addClass("hidden");
	}
	if(SLIDE.ACTIVE == 7 || SLIDE.ACTIVE == 9){ 
		$("#sidebar-decorations").addClass("hidden");
		$("#option-none").removeClass("hidden");
	}
	if(SLIDE.ACTIVE == 9){ 
		customTrollPositioning("solo-troll");
		$("#troll-shadow").fadeIn();
		$("#pod-shadow").fadeOut();
		$("#asset-display-zone").removeClass("position-2").addClass("position-1");
		$(".pod-asset").addClass("hidden");
	}
	if(SLIDE.ACTIVE == 10){ 
		$("#troll-shadow").addClass("hidden");
		$("#pod-shadow").removeClass("hidden").addClass("visible");
	}
	if(SLIDE.ACTIVE == 11){ 
		$("#asset-display-zone").removeClass("position-3").addClass("position-2");
		$(".vines").removeClass("position-2").addClass("position-1");
		$(".name-asset").addClass("hidden");
	}
	if(SLIDE.ACTIVE == 12){ 
		$stepForward.removeClass("force-exit hidden");
	}
	if(SLIDE.ACTIVE == 13){ 
		customTrollPositioning("pod-troll");
		removeTrollPositioning("preview-poster");
		$("#asset-display-zone").css("display","block");
		$("#preview-poster, #coloring-page").removeClass("preview");
		$(".vines").removeClass("hidden");
		$(".poster-preview").removeClass("reveal");
		$("#final-step-loading").hide();
		$("#final-step-pre").show();
		delayNavigation("entry",1500);
	}
}

function returnToStart(){ 
	$("#preview-poster, #coloring-page").removeClass("preview");
	$(".poster-preview").removeClass("reveal");
	$("#final-step-loading").hide();
	$("#final-step-pre").show();
	$("#asset-display-zone").removeClass("position-2").addClass("position-1");

	$("#girl").removeClass("selected");
	$("#boy").removeClass("selected");
	$("#boy .glow, #girl .glow").css("display","block");

	delayNavigation("exit",0);
	setTimeout(function(){
		$("#step-backward").css("display", "none");
	}, 1000);

	resetPosterPreviewPosition();
	leaveCurrentSlide();
	setTimeout(function(){
		SLIDE.JUMPBACKWARD(0);
		$(".pod-asset, .name-asset, #pod-acc").addClass("hidden");
	}, 1490);

	resetVisitedSlides();

	RESTART = true;
}

function resetVisitedSlides(){ 
	// Generate visited slide variables for option slides.
	// These variables are used to set and reveal character assets.
	for (i = 0; i < 12; i++){ 
		SLIDE['VISITED'][i] = false;
	}
}

function assetReveal(optionNum){ 
	var optionString = optionNum.toString(); 

	if(SLIDE['VISITED'][optionNum] == false){
		updateCharacterAssets(optionNum,1); // character.js
		SLIDE['VISITED'][optionNum] = true;
	}

	if(optionNum == 4){ 
		$("#expression").removeClass("hidden");
	} else if(optionNum == 7){ 
		$("#hair-acc").removeClass("hidden"); 
	} else if(optionNum == 8){ 
		$("#outfit-front, #outfit-back").removeClass("hidden");
	} else if(optionNum == 9){ 
		$("#attire-acc").removeClass("hidden");
	} else if(optionNum == 11){ 
		$("#pod-acc").removeClass("hidden");
	}	
}

function delayNavigation(direction, delay){ 
	setTimeout(function(){ 
		if(direction == "entry"){ 
			$("#step-forward, #step-backward").addClass("enter").removeClass("exit");
		} else if(direction == "exit"){ 
			$("#step-forward, #step-backward").removeClass("enter").addClass("exit");
		}
	}, delay);
}

function leaveCurrentSlide(){ 
	var $currentSlide = $('div.active-slide[data-slide='+ SLIDE.ACTIVE +']'); 
	$currentSlide.addClass("exit");
}

function resetPosterPreviewPosition(){ 
	$(".poster-preview").removeClass("reveal").addClass("exit");
	setTimeout(function(){
		$("poster-preview").removeClass("exit");
	}, 1500);
}

function trollGenderSlideExit(direction){ 
	$("#girl .glow, #boy .glow").css({"opacity": 0, "transform":"scale(0.8)"});
	
	if(!$("#girl").hasClass("selected") && !$("#boy").hasClass("selected")){
		if(direction == "next"){
		// 	$("#girl .glow").css({"opacity": 1, "transform":"scale(1)"});
		// 	setTimeout(function(){
		// 		$("#girl .glow").css({"opacity": 0, "transform":"scale(0.8)"});
		// 	}, 500);
		// 	$("#girl .bg-display-item").css("background-image", "url(img/generic-troll/girl-hover.svg)");
			$("#girl").addClass("selected");
			$("#boy .glow").css("display","none");
		}
	} 

	if($(".exit #girl").hasClass("selected")){
		document.getElementById("boy-image").style.WebkitAnimationName = "first-bounceOutUp";
		document.getElementById("boy-image").style.animationName = "first-bounceOutUp";

		document.getElementById("girl-image").style.WebkitAnimationName = "second-bounceOutUp";
		document.getElementById("girl-image").style.animationName = "second-bounceOutUp";

	} else if($(".exit #boy").hasClass("selected")){ 
		document.getElementById("boy-image").style.WebkitAnimationName = "second-bounceOutUp";
		document.getElementById("boy-image").style.animationName = "second-bounceOutUp";

		document.getElementById("girl-image").style.WebkitAnimationName = "first-bounceOutUp";
		document.getElementById("girl-image").style.animationName = "first-bounceOutUp";

	} else { 
		document.getElementById("boy-image").style.WebkitAnimationName = "first-bounceOutUp";
		document.getElementById("boy-image").style.animationName = "first-bounceOutUp";

		document.getElementById("girl-image").style.WebkitAnimationName = "second-bounceOutUp";
		document.getElementById("girl-image").style.animationName = "second-bounceOutUp";
	}
}

function optionSlideExit(){ 
	$(".option-screens").addClass("exit");
	setTimeout(function(){
		$(".option-screens").removeClass("exit");
	}, 1500);
}

function assetZoneExit(){ 
	$("#asset-display-zone, .pod-asset.vines.set-1, .pod-asset.vines.set-2").addClass("exit");
	$("#step-forward > a.hit-box, #step-backward > a.hit-box").css("pointer-events", "none");

	setTimeout(function(){
			$("#asset-display-zone, .pod-asset.vines.set-1, .pod-asset.vines.set-2").removeClass("exit");
			$("#step-forward > a.hit-box, #step-backward > a.hit-box").css("pointer-events", "auto");
	}, 1490); 
}

function posterPreviewExit(){ 
	$(".poster-preview").addClass("exit");
	setTimeout(function(){
			$(".poster-preview").removeClass("exit");
	}, 1500); 	
}

function generatePDFslideExit(){ 
	$(".poster-preview").removeClass("exit").addClass("reveal");
	$(".pod-asset, .name-asset").removeClass("hidden");
	$(".pod-asset.vines").addClass("hidden");
	$('div.active-slide[data-slide=13], #asset-display-zone').addClass("exit linger");
	setTimeout(function(){
		$('div.active-slide[data-slide=13], #asset-display-zone').removeClass("exit linger");
	}, 1500);
}

function loadingScreenExit(){ 
	$("#assets-loaded, #loading-text").addClass("exit");
	$("#assets-loaded").css("overflow", "visible");
	$("#assets-not-loaded").css("display", "none");
	setTimeout(
		function(){$("#assets-loaded, #assets-not-loaded, #loading-text").addClass("hidden");

		setTimeout(function(){ 
			// Show the first slide! 
			$(".make-volume, #first-screen").removeClass("hidden");
			$("#first-screen").addClass("active-slide");
		}, 250);
	}, 2000); 

}

function disableNav(){ 
	$("#step-forward > a.hit-box, #step-backward > a.hit-box").css("pointer-events", "none");
}

function enableNav(){ 
	setTimeout(function(){ 
		$("#step-forward > a.hit-box, #step-backward > a.hit-box").css("pointer-events", "auto");
	}, 250);
}
/////////////////////////////////////////
// UTILITY FUNCTIONS
/////////////////////////////////////////

/* WINDOW RESIZING FUNCTIONS
----------------------------------- */
function bindWindowSizeEvent(func)
{
	$(window).on("debouncedresize", func);  
	$(window).on("orientationchange", function(){ setTimeout(func, 150);});
	func();
}
function bindIntereactionEvent(func)
{
	$(window).on("debouncedresize scroll touchstart touchmove touchend", func);
	$(window).on("orientationchange", function(){ setTimeout(func, 150);});
	func();
}