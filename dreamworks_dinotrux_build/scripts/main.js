var curr_slide = 0, sliding = false;;
		
$(document).ready(function(){

	FastClick.attach(document.body);
	
	//Toggle audio play
	var music = new Audio('assets/sound/sound.mp3');

	$('#play').click(function() {
		$('#play').css('display','none');
		$('#pause').css('display','block');
		music.play();
	});

	$('#pause').click(function() {
		$('#pause').css('display','none');
		$('#play').css('display','block');
		music.pause();
	});

	// Start Button
	$('div#welcome div.action div.start').on('click', next_slide);

	// Arrows
	$('div#step-forward').on('click', next_slide);
	$('div#step-back').on('click', prev_slide);

	//Popup
	$("div.popup a.close").on("click", function(){ $("#shade, div.popup").toggleClass("open"); });

	bindWindowSizeEvent(function(){
		$("#stage").width("100%");
		$("#stage").height((9 * parseInt($("#stage").width())) / 16);

		$('div.slide').each(function(){
			// Set height and width of slide to "#stage" size, and position based on position to current slide
			if ($(this).data('slide') < curr_slide) {
				$(this).css({
					width : $("#stage").width(),
					height: $("#stage").height(),
					top: 0,
					left : -2 * parseInt($("#stage").width())
				});
			} else if ($(this).data('slide') > curr_slide){
				$(this).css({
					width : $("#stage").width(),
					height: $("#stage").height(),
					top: 0,
					left : 2 * parseInt($("#stage").width())
				});
			} else {
				$(this).css({
					width : $("#stage").width(),
					height: $("#stage").height(),
					top: 0,
					left: 0
				});
			}
		});
	});

});  //document.ready()





/////////////////////////////////////////
// Slider / UI Functions
/////////////////////////////////////////

// Next slide
function next_slide() {

	if(sliding) return;
	else sliding = true;

	// Animate current slide and next slide to the left
	$('div.slide[data-slide=' + curr_slide + '], div.slide[data-slide=' + (curr_slide + 1) + ']').css('display', 'block').stop(true, true).animate({
		"left" : "-=" + (2 * parseInt($("#stage").width())) + "px"
	}, 500, function(){
		sliding = false;
	});

	// Update current slide
	curr_slide++;

	//Show and hide character on certain steps
	if(curr_slide == $('div.slide').length - 1) {
		$('#main-character').fadeOut();
		$('#preview-character').fadeIn();
	} else if(curr_slide >= 3) {
		$('#main-character').fadeIn('fast');
	}

	// Update arrows
	$('div#step-back').fadeIn();
	if(curr_slide == $('div.slide').length - 1) $('div#step-forward').fadeOut();
	else $('div#step-forward').fadeIn();
	
	build_scene();
	update_active();

	//some additional flair animations
	if(curr_slide == 1) setTimeout(function(){ $("div#introduction div.sign").addClass("show"); }, 400);
}

// Prev slide
function prev_slide() {

	if(sliding) return;
	else sliding = true;

	// Animate current slide and prev slide to the right
	$('div.slide[data-slide=' + curr_slide + '], div.slide[data-slide=' + (curr_slide - 1) + ']').css('display', 'block').stop(true, true).animate({
		"left" : "+=" + (2 * parseInt($("#stage").width())) + "px"
	}, 500, function(){
		sliding = false;
	});

	// Update current slide
	curr_slide--;

	//Show and hide character on certain steps
	if(curr_slide < 3) {
		$('#main-character').fadeOut();
	} else if(curr_slide < $('div.slide').length - 1) {
		$('#preview-character').fadeOut();
		$('#main-character').fadeIn();
	}

	// Update arrows
	$('div#step-forward').fadeIn();
	if(curr_slide == 0) $('div#step-back').fadeOut();
	else $('div#step-back').fadeIn();

	build_scene();
	update_active();

	//hide final Ty
	$(".ty-copy, .ty_rivet_download").fadeOut('fast');

	//reset assets here
	$("#final-step-pre").show();
	$("#final-step-loading, #final-step-post").hide();
	doc = null;
	poster_image_landscape = null;
	poster_image_landscape_lores = null,
	poster_image_portrait = null;
	pdfready = false;
	awsready = false;
}

function update_active() {
	var section = $('div.slide[data-slide=' + curr_slide + ']').attr('id');

	$('div.slide[data-slide=' + curr_slide + '] div.options a').each(function(){
		$(this).removeClass('active');

		var current = character[section];
		if (current && $(this).data('id') == current) {
			$(this).addClass('active');
		}
	});
}

function goEnd() {
	$('div.slide, div.slide').css('left', -10000).css("display", "block");
	var lastslide = $('div.slide').length - 1;
	$('div.slide[data-slide=' + lastslide + ']').css("left", 0);
	curr_slide = lastslide;
}





/////////////////////////////////////////
// Utility Functions
/////////////////////////////////////////

function bindWindowSizeEvent(func)
{
	$(window).on("debouncedresize", func);  
	$(window).on("orientationchange", function(){ setTimeout(func, 150);});
	func();
}

function bindIntereactionEvent(func)
{
	$(window).on("debouncedresize scroll touchstart touchmove touchend", func);
	$(window).on("orientationchange", function(){ setTimeout(func, 150);});
	func();
}