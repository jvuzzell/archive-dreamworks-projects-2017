// File format and path used to select which image to load on front end
var format = 'svg';
var img_path = 'assets/dinotrux/' + format + '/';

// Create character object used to draw scene and set defaults
var character = {};
character.species = 1;
character.color = 2;
character.body = 1;
character.feet = 5;
character.tail = 4;
character.expression = 1;
character.condition = 0;
character.friend = 0;
character.prop = 0;
character.background = 1;
character.name1;
character.name2;
character.name3;

var names = [
  ['Anklyo', 'Brachio' , 'Stegar', 'Tricera', 'Tyranno' , 'Veloco', 'Crane', 'Dozer' , 'Drill', 'Dump', 'Loader' , 'Mega ton'],
  ['adon', 'asaur' , 'atops', 'ator', 'aurus' , 'basaur', 'osaur', 'saurus' , 'tops', 'crane', 'dozer' , 'drill', 'dump', 'loader', 'mega ton'],
  [' Trux', 'adon' , 'asaur', 'atops', 'ator' , 'aurus', 'basaur', 'osaur' , 'saurus', 'tops', '']
];

function randomizeNames() {
  character.name1 = names[0][Math.floor(Math.random() * names[0].length)];
  character.name2 = names[1][Math.floor(Math.random() * names[1].length)];
  character.name3 = names[2][Math.floor(Math.random() * names[2].length)];
  drawNames();
}

function incrementName() {
  var which = $(this).data('which');
  var whichindex = which - 1;
  var currIndex, nextIndex;

  for(var i = 0; i < names[whichindex].length; i++) {
    if(names[whichindex][i] == character["name" + which]) {
      currIndex = i;
    }
  }

  nextIndex = currIndex + 1;
  if(nextIndex >= names[whichindex].length) nextIndex = 0;

  character["name" + which] = names[whichindex][nextIndex];
  drawNames();
}

function drawNames() {
  $("#name-1").html(character.name1);
  $("#name-2").html(character.name2);
  $("#name-3").html(character.name3 == "" ? '(none)' : character.name3);
  $('div#name, div#print div.name').html(character.name1 + character.name2 + character.name3);
}




/////////////////////////////////////////
// Document.ready
/////////////////////////////////////////

$(document).ready(function(){

  randomizeNames();

  $("div.name div.options .refresh").on("click", incrementName);

  $('div#species div.options a').on('click', function(e){
    e.preventDefault;
    character.species = $(this).data('id');

    switch(character.species) {
      case 1 : //tyrannosauraus
        character.color = 2;
        character.body = 1;
        character.feet = 5;
        character.tail = 4;
        break;
      case 2 : //cranseosaur
        character.color = 4;
        character.body = 2;
        character.feet = 3;
        character.tail = 5;
        break;
      case 3 : //stegarbasaur
        character.color = 3;
        character.body = 3;
        character.feet = 1;
        character.tail = 2;
        break
      case 4 : //dozeratops
        character.color = 1;
        character.body = 4;
        character.feet = 2;
        character.tail = 3;
        break
      case 5 : //anklyodump
        character.color = 5;
        character.body = 5;
        character.feet = 4;
        character.tail = 1;
        break
    }
    next_slide();
  });

  $('div#color div.options a').on('click', function(e){
    e.preventDefault;
    character.color = $(this).data('id');
    build_scene();
  });

  $('div#body div.options a').on('click', function(e){
    e.preventDefault;
    character.body = $(this).data('id');
    build_scene();
  });

  $('div#feet div.options a').on('click', function(e){
    e.preventDefault;
    character.feet = $(this).data('id');
    build_scene();
  });

  $('div#tail div.options a').on('click', function(e){
    e.preventDefault;
    character.tail = $(this).data('id');
    build_scene();
  });

  $('div#expression div.options a').on('click', function(e){
    e.preventDefault;
    character.expression = $(this).data('id');
    build_scene();
  });

  $('div#condition div.options a').on('click', function(e){
    e.preventDefault;
    character.condition = $(this).data('id');
    build_scene();
  });

  $('div#friend div.options a').on('click', function(e){
    e.preventDefault;
    character.friend = $(this).data('id');
    build_scene();
    //some additional flair animations
    if(character.friend) {
      $("div.character img.part-friend").addClass("falling");
      setTimeout(function(){ $("div.character img.part-friend").removeClass("airborne"); }, 800);
    }
  });

  $('div#prop div.options a').on('click', function(e){
    e.preventDefault;
    character.prop = $(this).data('id');
    build_scene();
    //some additional flair animations
    if(character.prop) {
      $("div.character img.part-prop").addClass("falling");
      setTimeout(function(){ $("div.character img.part-prop").removeClass("airborne"); }, 800);
    }
  });

  $('div#background div.options a').on('click', function(e){
    e.preventDefault;
    character.background = $(this).data('id');
    build_scene();
  });
  
});





/////////////////////////////////////////
// Build Scene
/////////////////////////////////////////

var requiredAssetsRemaining = [];
var timeout;

setInterval(function(){

  var i = requiredAssetsRemaining.length;

  while(i--) {
    if(assets[requiredAssetsRemaining[i]]) {
      requiredAssetsRemaining.splice(i, 1);
    }
  }

  var now = new Date();
  if(requiredAssetsRemaining.length == 0 || now - timeout > 10000) {
    $("#stage").removeClass("loading");
  }

}, 500);



function build_scene() {

  incrementalPreload();
  update_active();

  var requiredAssetsForScene = [];
  
  //background
  $('#stage, div#print div.poster').css({
    "background-image" : "url(assets/imgs/backgrounds/bg_" + character.background + ".jpg)"
  });

  //change out ui icons to reflect color and species
  $('.slide .options a img').each(function(){

    var oldsrc = $(this).get(0).src;
    var newsrc = oldsrc.replace(/color\-\d/, 'color-' + character.color);
    
    if($(this).hasClass('is-expression')) {
      newsrc = newsrc.replace(/head\-\d+/, 'head-' + (4 * (character.species - 1) + $(this).data('count')));
      $(this).attr('id', 'expression-button-' + $(this).data('count'));
    }

    if(newsrc != oldsrc) {
      $(this).get(0).src = newsrc;
      requiredAssetsForScene.push($(this).get(0).src);
    }

  });

  //dinoparts
  $("div.character img").each(function(){

    var oldsrc, newsrc;
    
    //body
    if($(this).hasClass('part-body')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/color-' + character.color + '.' + format;
      if(newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    } 

    //head
    else if($(this).hasClass('part-head')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/color-' + character.color + '/head-' + (character.expression  + (4 * (character.species - 1))) + '.' + format;
      if(newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //leg front
    else if($(this).hasClass('part-leg-front')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/color-' + character.color + '/leg-front-' + character.feet + '.' + format;
      if(newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //leg rear
    else if($(this).hasClass('part-leg-rear')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/color-' + character.color + '/leg-rear-' + character.feet + '.' + format;
      if(newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //tail
    else if($(this).hasClass('part-tail')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/color-' + character.color + '/tail-' + character.tail + '.' + format;
      if(newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //friend
    else if($(this).hasClass('part-friend')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/friend-' + character.friend + '.' + format;
      if(character.friend && newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //prop
    else if($(this).hasClass('part-prop')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/prop-' + character.prop + '.' + format;
      if(character.prop && newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //body condition
    if($(this).hasClass('part-body-condition')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/condition-' + character.condition + '/body-' + character.body + '.' + format;
      if(character.condition && newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    } 

    //head condition
    else if($(this).hasClass('part-head-condition')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/condition-' + character.condition + '/head-' + character.species + '.' + format;
      if(character.condition && newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //leg front condition
    else if($(this).hasClass('part-leg-front-condition')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/condition-' + character.condition + '/leg-front-' + character.feet + '.' + format;
      if(character.condition && newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //leg rear condition
    else if($(this).hasClass('part-leg-rear-condition')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/condition-' + character.condition + '/leg-rear-' + character.feet + '.' + format;
      if(character.condition && newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    //tail condition
    else if($(this).hasClass('part-tail-condition')) {
      oldsrc = $(this).get(0).src;
      newsrc = img_path + 'web/body-' + character.body + '/condition-' + character.condition + '/tail-' + character.tail + '.' + format;
      if(character.condition && newsrc != oldsrc) {
        $(this).get(0).src = newsrc;
        requiredAssetsForScene.push($(this).get(0).src);
      }
    }

    ////////////////////////////////////////////////////////////////////

    //hide/show conditions if enabled
    if($(this).hasClass("condition")) {
      if(character.condition) $(this).show();
      else $(this).hide();
    } 

    //hide/show prop if enabled
    else if($(this).hasClass("part-friend")) {
      if(character.friend) $(this).show();
      else $(this).hide();
    } 

    //hide/show friend if enabled
    else if($(this).hasClass("part-prop")) {
      if(character.prop) $(this).show();
      else $(this).hide();
    }

  });

  //do we need to show the loader?

  var i = requiredAssetsForScene.length;

  while(i--) {
    if(assets[requiredAssetsForScene[i]]) {

      //image already loaded
      requiredAssetsForScene.splice(i, 1);

    } else {

      //image needs to be loaded and removed from the manifest
      var img = new Image();
      img.onload = registerAsset;
      img.onerror = preloadFailed;
      img.src = requiredAssetsForScene[i];

      var j = manifest.dinoparts.length;
      while(j--) {
        if(manifest.dinoparts[j] == requiredAssetsForScene[i].replace(location.href, '')) {
          manifest.dinoparts.splice(j, 1);
        }
      }

    }
  }

  if(requiredAssetsForScene.length > 0) {
    requiredAssetsRemaining = requiredAssetsForScene;
    timeout = new Date();
    $("#stage").addClass("loading");
  }

}
