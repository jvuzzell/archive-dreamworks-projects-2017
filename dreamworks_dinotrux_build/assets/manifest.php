<?php

//http://php.net/manual/en/class.recursivedirectoryiterator.php#97228

$Manifest = array(
  "staticui" => array(),
  "dynamicui" => array(),
  "dinoparts" => array()
);

$Directory = new RecursiveDirectoryIterator('.');
$Iterator = new RecursiveIteratorIterator($Directory);

//sort function

function assetSort($a, $b) {
  if(preg_match("/leg\-/", $a) && preg_match("/leg\-/", $b)) {
    return 0;
  } else if(preg_match("/leg\-/", $a)) {
    return 1;
  } else if(preg_match("/leg\-/", $b)) {
    return -1;
  } else if(preg_match("/tail\-/", $a) && preg_match("/tail\-/", $b)) {
    return 0;
  } else if(preg_match("/tail\-/", $a)) {
    return 1;
  } else if(preg_match("/tail\-/", $b)) {
    return -1;
  } else if(preg_match("/head\-/", $a) && preg_match("/head\-/", $b)) {
    return 0;
  } else if(preg_match("/head\-/", $a)) {
    return 1;
  } else if(preg_match("/head\-/", $b)) {
    return -1;
  } else {
    return 0;
  }
}

//site assets

$StaticUIRegex = new RegexIterator($Iterator, '/^.+imgs.+\.(jpg|png)$/i', RecursiveRegexIterator::GET_MATCH);
$StaticUIArray = array_keys(iterator_to_array($StaticUIRegex));
$Manifest["staticui"] = array_merge($Manifest["staticui"], $StaticUIArray);
$Manifest["staticui"] = preg_replace('/^\./', 'assets', $Manifest["staticui"]);

//ui icons

$DynamicUIRegex = new RegexIterator($Iterator, '/^.+dinotrux\/png\/icon.+\.png$/i', RecursiveRegexIterator::GET_MATCH);
$DynamicUIArray = array_keys(iterator_to_array($DynamicUIRegex));
$Manifest["dynamicui"] = array_merge($Manifest["dynamicui"], $DynamicUIArray);
$Manifest["dynamicui"] = preg_replace('/^\./', 'assets', $Manifest["dynamicui"]);

usort($Manifest["dynamicui"], "assetSort");

//svg parts

$DinopartsRegex = new RegexIterator($Iterator, '/^.+dinotrux\/svg\/web.+\.svg$/i', RecursiveRegexIterator::GET_MATCH);
$DinopartsArray = array_keys(iterator_to_array($DinopartsRegex));
$Manifest["dinoparts"]= array_merge($Manifest["dinoparts"], $DinopartsArray);
$Manifest["dinoparts"] = preg_replace('/^\./', 'assets', $Manifest["dinoparts"]);

usort($Manifest["dinoparts"], "assetSort");

//output

$json = json_encode($Manifest, JSON_UNESCAPED_SLASHES);
file_put_contents("manifest.json", $json);

?>