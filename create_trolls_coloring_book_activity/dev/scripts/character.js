var WEB_PREFIX = "assets/web/svg/";
var WEB_SUFFIX = ".svg";

var PRINT_PREFIX = "assets/print/png/";
var PRINT_LORES_PREFIX = "assets/print/png-lores/";
var PRINT_SUFFIX = ".png";

var POSTER_PREFIX = "assets/web/png/";
var POSTER_LORES_PREFIX = "assets/web/png-lores/";

var PARTNUM = {};
var TROLL = {};
var POD = {};
var NAME = {};

var NAMESELECTED = false;

// These variables control whether we display 
// selected outfits and accessories
var PREVIOUSGENDER;
var GENDERSELECTED = false; 
var DISPLAYSELECTED = false;
var FIRSTTIME = true;
var HARDRESET = false;

// Used to hide back button
var SPECIALCASE = true;

function initCharacterGlobals(gender){
	// never receive 0 or undefined for gender
	if (!gender){console.log("Error: initCharacterGlobals - gender undefined"); return}

	// '0' means not selected and the value is used 
	// later to screen what assets to display in the 
	// browser. 
	var selection = "";
	
	if(HARDRESET == true){selection = 1;}

	PARTNUM = { 
		name1 : 1, // First Name
		name2 : 1, // Second Name
		name3 : 1, // Third Name
		d : gender, // Gender
		e : 1, // Pose
		f : selection, // Expression
		g : selection, // Hair Style
		h : selection, // Hair Color
		i : selection, // Hair Accessory
		j : selection, // Attire
		k : selection, // Attire Accessory
		l : 1, // Pod
		m : 1  // Pod Accessory
	}
	NAME = { 
		name1 : "", // string
		name2 : "", // string
		name3 : "",  // string
		paths : { 
			a : "", // First Name
			b : "", // Second Name
			c : ""  // Third Name
		}
	}
	TROLL = { 
		d : "", // Gender
		e : "", // Pose
		f : "", // Expression
		// Purposefully omitted TROLL.g
		h : "", // Hair Color
		i : "", // Hair Accessory
		k : "", // Attire Accessory
		j_front : "", // Attire Front
		j_back  : "" // Attire Back
	}
	POD = { 
		l_front : "", // Pod Front
		l_back : "", // Pod back
		m : ""  // Pod Accessory
	}

	// Reset visited slides
	for(i = 4; i < 12; i++){ 
		SLIDE['VISITED'][i] = false;
	}
}

function initSelections(gender){ 
	// This function is called when a new gender is selected 
	// by the user
	// 1) Moves the selected icon to position if a new gender
	//    has been selected.
	// 2) Changes option icons based on gender of troll
	// 3) Changes assets displayed in the browser to reflect
	//    those belonging to the related troll's gender

	// Reset the UI "Selected Icon" to first position 
	$allSelectedIcons = $(".slide-container > .slide .option-wrap .selected");
	$allSelectedIcons.removeClass();

	$allSelectedIcons.addClass("position-1 bg-img selected");

	// Switch UI icon sets based on gender
	var a = "";
	var b = "";
	var num = "";
	var option = "";
	var i = 1;
	var n = 1; 
	var icon = "";
	var newIcon = "";

	if(gender == 1){ 
		a = "girl";
		b = "boy";
	} else if (gender == 2){ 
		a = "boy";
		b = "girl";
	}

	for (i = 2; i < 10; i++){ 
		num = i.toString();

		for (n = 1; n < 5; n++){ 
			num2 = n.toString();
			option = "#option_" + num + " .options a:nth-child("+ num2 +") > div";

			// Replace Boy and Girl option icons  
			// based on user gender selection
			icon = $(option).css("background-image");
			newIcon = icon.replace(b, a); 
			$(option).css("background-image", newIcon);
			
		}
	}	

	// Change gender of troll displayed in browser
	updateCharacterAssets(2, gender);

}

function initGENDER(gender){
	// if no gender then set default
	if (!gender){
		gender = 1;
	}
	if (PREVIOUSGENDER == gender){ 
		if(RESTART == true){ 
			PREVIOUSGENDER = gender;
			DISPLAYSELECTED = false; 
			HARDRESET = true;
			SPECIALCASE = true;

			initCharacterGlobals(gender);
			initSelections(gender);
			randomizeNames();

			RESTART = false;
		} else {
			PREVIOUSGENDER = gender;
			DISPLAYSELECTED = true; 
			HARDRESET = false;
		}
	} else {
		PREVIOUSGENDER = gender;
		DISPLAYSELECTED = false;
		HARDRESET = true; 

		initCharacterGlobals(gender);
		initSelections(gender);
		randomizeNames();
	}
}


$(document).ready(function(){
	/* CHARACTER SELECT INTERFACE
	------------------------------- */
	$("#girl .hit-box").on("click", function(){
		$("#boy .bg-display-item").css("background-image", "url(img/generic-troll/boy.svg)");
		$("#boy .glow").css({"opacity": 0, "transform":"scale(0.8)", "display":"none"});
		$("#boy").removeClass("selected");
		$("#girl .bg-display-item").css("background-image", "url(img/generic-troll/girl-hover.svg)");
		$("#girl .glow").css({"opacity": 1, "transform":"scale(1)", "display":"block"});
		$("#girl").addClass("selected");
		PARTNUM.d = 1;
		grantPassage();
	});
	$("#boy .hit-box").on("click", function(){
		$("#girl .bg-display-item").css("background-image", "url(img/generic-troll/girl.svg)");
		$("#girl .glow").css({"opacity": 0, "transform":"scale(0.8)", "display":"none"});
		$("#girl").removeClass("selected");
		$("#boy .bg-display-item").css("background-image", "url(img/generic-troll/boy-hover.svg)");
		$("#boy .glow").css({"opacity": 1, "transform":"scale(1)", "display":"block"});
		$("#boy").addClass("selected");
		PARTNUM.d = 2;
		grantPassage();
	});

	function grantPassage(){ 
		delayNavigation("entry",200);
		setTimeout(function(){$("#step-forward").addClass('hover');}, 250);
		if(SPECIALCASE === true){ 
			$("#step-backward").css("display","none").addClass("force-exit");
			SPECIALCASE = false;
		}
	}

	////////////////////////////////////////////////////
	// Update Option Icons - ACTIVE STATES 
	// * Collect selection data from each option
	///////////////////////////////////////////////////
	$(".options > a").on("click", function(){
		$selected = $(this);
		var selection = $selected.data('selection');

		$choices = $selected.parents(".slide"); 

		// Trigger Asset Display 
		updateCharacterAssets($choices.data('slide'), selection);
		
		// Update Icon
		$selectedIcon = $choices.find(".option-wrap .selected");
	
		$selectedIcon.removeClass();
		$selectedIcon.addClass("position-" + selection);
		$selectedIcon.addClass("bg-img selected");	
	});	

	/////////////////////////////////////////
	// Update Option Icons - HOVER STATES
	/////////////////////////////////////////
    $('.options > a[data-selection].option').hover(function() {
        $(this).siblings('a').addClass("fadeOutOptions");
  
    }, function() {
        $(this).siblings('a').removeClass("fadeOutOptions");
    });

    /////////////////////////////////////////
	// Update Option Icons - HOVER STATES
	/////////////////////////////////////////   
    $("#option_11 .forward, #name-1, #name-2, #name-3").on("click", {option:"increment"}, moveThroughNames); 
    $("#option_11 .backward").on("click", {option:"decrement"}, moveThroughNames); 
    $("#refresh").on("click", randomizeNames);

    /*-------------------------------*/

});

/* CHARACTER BUILDER
------------------------------- */
function updateCharacterAssets(option, selection){
	// NOTE: Option variable here uses slide number to determine 
	// what is character assets are meant to change.
	switch (option){ 
		case 2: 
			PARTNUM.d = selection; //Gender
		break;
		case 3: 
			PARTNUM.e = selection; //Pose
		break;
		case 4: 
			PARTNUM.f = selection; //Expression
		break; 
		case 5: 
			PARTNUM.g = selection; //Hair Style
		break; 
		case 6: 
			PARTNUM.h = selection; //Hair Color
		break; 
		case 7: 
			PARTNUM.i = selection; //Hair Accessory
		break; 
		case 8: 
			PARTNUM.j = selection; //Outfit
		break; 
		case 9: 
			PARTNUM.k = selection; //Attire Accessory
		break; 
		case 10: 
			PARTNUM.l = selection; //Pod
		break; 
		case 11: 
			PARTNUM.m = selection; //Pod Accessory
		break;
	}

    if(PARTNUM.d != 0){TROLL.d   = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/pose";} //gender
	if(PARTNUM.e != 0){TROLL.e   = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/pose"; if(SLIDE.ACTIVE == 2 || SLIDE.ACTIVE == 3){customTrollPositioning("solo-troll");}} //pose
	if(PARTNUM.f != 0){TROLL.f   = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/expression/expression-"+ PARTNUM.f;} //expression
	if(PARTNUM.h != 0){TROLL.h   = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/hair/style-"+ PARTNUM.g +"/style-"+ PARTNUM.g +"-color-"+ PARTNUM.h;} //hair style
	if(PARTNUM.h != 0){TROLL.h   = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/hair/style-"+ PARTNUM.g +"/style-"+ PARTNUM.g +"-color-"+ PARTNUM.h;} //hair color
	if(PARTNUM.i != 0){TROLL.i   = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/hair/style-"+ PARTNUM.g +"/style-"+ PARTNUM.g +"-acc-"+ PARTNUM.i;} //hair accessory
	else if(PARTNUM.i == 0){PARTNUM.i = 0; TROLL.i = undefined;} //No hair accessory
	if(PARTNUM.j != 0){TROLL.j_front  = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/attire/outfit-"+ PARTNUM.j +"/outfit-a-"+ PARTNUM.j;} //outfit front
	if(PARTNUM.j != 0){TROLL.j_back   = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/attire/outfit-"+ PARTNUM.j +"/outfit-b-"+ PARTNUM.j;} //outfit back
	if(PARTNUM.k != 0){TROLL.k     = "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/attire/attire-acc-"+ PARTNUM.k;} //attire accessory
	else if(PARTNUM.k == 0){PARTNUM.k = 0; TROLL.k = undefined;} //No attire accessory 
	if(PARTNUM.l != 0){POD.l_front = "gender-"+ PARTNUM.d +"/pods/pod-"+ PARTNUM.l +"/pod-"+ PARTNUM.l +"-pod-a";} //pod front
	if(PARTNUM.l != 0){POD.l_back  = "gender-"+ PARTNUM.d +"/pods/pod-"+ PARTNUM.l +"/pod-"+ PARTNUM.l +"-pod-b";} //pod back
	if(PARTNUM.m != 0){POD.m       ="gender-"+ PARTNUM.d +"/pods/pod-"+ PARTNUM.l +"/pod-"+ PARTNUM.l +"-acc-"+ PARTNUM.m;} //pod accessory

	browserDisplayCharacter();

}

function browserDisplayCharacter(){
	
	// In order of appearance in DOM
	if(PARTNUM.l != 0){
		$("#pod, #preview-poster-pod").attr("src", WEB_PREFIX+ POD.l_back +WEB_SUFFIX);
		$("#coloring-page-pod").attr("src", PRINT_PREFIX+ POD.l_back +PRINT_SUFFIX);

	}
	if(PARTNUM.m != 0){
		$("#pod-acc, #preview-poster-pod-acc").attr("src", WEB_PREFIX+ POD.m +WEB_SUFFIX);
		$("#coloring-page-pod-acc").attr("src", PRINT_PREFIX+ POD.m +PRINT_SUFFIX);
	
	}
	if(PARTNUM.d != 0){
		$("#pose, #preview-poster-pose").attr("src", WEB_PREFIX+ TROLL.d +WEB_SUFFIX);
		$("#coloring-page-pose").attr("src", PRINT_PREFIX+ TROLL.d +PRINT_SUFFIX);
		
	}
	if(PARTNUM.j != 0){
		$("#outfit-back, #preview-poster-outfit-back").attr("src", WEB_PREFIX+ TROLL.j_back +WEB_SUFFIX);
		$("#coloring-page-outfit-back").attr("src", PRINT_PREFIX+ TROLL.j_back +PRINT_SUFFIX);
		
	}
	if(PARTNUM.f != 0){
		$("#expression, #preview-poster-expression").attr("src", WEB_PREFIX+ TROLL.f +WEB_SUFFIX);
		$("#coloring-page-expression").attr("src", PRINT_PREFIX+ TROLL.f +PRINT_SUFFIX);
	
	}
	if(PARTNUM.j != 0){
		$("#outfit-front, #preview-poster-outfit-front").attr("src", WEB_PREFIX+ TROLL.j_front +WEB_SUFFIX);
		$("#coloring-page-outfit-front").attr("src", PRINT_PREFIX+ TROLL.j_front +PRINT_SUFFIX);
	
	}
	if(PARTNUM.h != 0){
		// Show generic hair until user gets to hair style slide
		if(SLIDE['VISITED'][5] == false){ 
			if(SLIDE.ACTIVE == 5){ 
				$("#hair, #preview-poster-hair").attr("src", WEB_PREFIX+ TROLL.h +WEB_SUFFIX);
				$("#coloring-page-hair").attr("src", PRINT_PREFIX+ TROLL.h +PRINT_SUFFIX);
			
			} else { 
				$("#hair").attr("src", WEB_PREFIX + "gender-"+ PARTNUM.d +"/troll/pose-"+ PARTNUM.e +"/hair/style-0-color-1.svg");
		
			}
		} else {
			$("#hair, #preview-poster-hair").attr("src", WEB_PREFIX+ TROLL.h +WEB_SUFFIX);
			$("#coloring-page-hair").attr("src", PRINT_PREFIX+ TROLL.h +PRINT_SUFFIX);
	
		}	
	}
	if(PARTNUM.i != 0){
		$("#hair-acc, #preview-poster-hair-acc").attr("src", WEB_PREFIX+ TROLL.i +WEB_SUFFIX);
		$("#coloring-page-hair-acc").attr("src", PRINT_PREFIX+ TROLL.i +PRINT_SUFFIX);
		if(SLIDE.ACTIVE == 7){ 
			$("#hair-acc, #preview-poster-hair-acc, #coloring-page-hair-acc").removeClass("hidden");
		}

	} else if(PARTNUM.i == 0){ 
		$("#hair-acc, #preview-poster-hair-acc").attr("src", "");
		$("#coloring-page-hair-acc").attr("src", "");
		if(SLIDE.ACTIVE == 7){ 
			$("#hair-acc, #preview-poster-hair-acc, #coloring-page-hair-acc").addClass("hidden");
		}
	}
	if(PARTNUM.k != 0){
		$("#attire-acc, #preview-poster-attire-acc").attr("src", WEB_PREFIX+ TROLL.k +WEB_SUFFIX);
		$("#coloring-page-attire-acc").attr("src", PRINT_PREFIX+ TROLL.k +PRINT_SUFFIX);
		if(SLIDE.ACTIVE == 9){ 
			$("#attire-acc, #preview-poster-attire-acc, #coloring-page-attire-acc").removeClass("hidden");
		}
	
	} else if(PARTNUM.k == 0){ 
		$("#attire-acc, #preview-poster-attire-acc").attr("src", "");
		$("#coloring-page-attire-acc").attr("src", "");
		if(SLIDE.ACTIVE == 9){ 
			$("#attire-acc, #preview-poster-attire-acc, #coloring-page-attire-acc").addClass("hidden");
		}
	}
	if(PARTNUM.l != 0){
		$("#pod-leaves, #preview-poster-pod-leaves").attr("src", WEB_PREFIX+ POD.l_front +WEB_SUFFIX);
		$("#coloring-page-pod-leaves").attr("src", PRINT_PREFIX+ POD.l_front +PRINT_SUFFIX);
	
	}

	//characterAssetLoader();

}

// Custom character placement
function customTrollPositioning(context){
	removeTrollPositioning("pod-troll");
	removeTrollPositioning("solo-troll");
	removeTrollPositioning("preview-poster");

	if(context == "solo-troll"){ 
		if(PARTNUM.d == 2){$(".troll-asset").addClass("position-1");}
		
		if( PARTNUM.d == 2 && PARTNUM.e == 2 ||
			PARTNUM.d == 2 && PARTNUM.e == 3 ||
			PARTNUM.d == 2 && PARTNUM.e == 4 
		){ 
			$(".troll-asset").addClass("position-6");
		}

		if(PARTNUM.d == 1 && PARTNUM.e == 1){ $(".troll-asset").addClass("position-5");}
		if(PARTNUM.d == 1 && PARTNUM.e == 2){ $(".troll-asset").addClass("position-2");}
		if(PARTNUM.d == 1 && PARTNUM.e == 3){ $(".troll-asset").addClass("position-3");}
		if(PARTNUM.d == 1 && PARTNUM.e == 4){ $(".troll-asset").addClass("position-4");}
	
	} else if (context == "pod-troll"){ 
		if(PARTNUM.d == 1){ 
			$(".troll-asset").addClass("position-7");
		} else if(PARTNUM.d == 2){ 
			$(".troll-asset").addClass("position-8");
		} 
		
	} else if(context == "preview-poster"){ 
		if(PARTNUM.l == 2){ 
			$("#preview-poster").addClass("position-2");
		}
	}
}	

function removeTrollPositioning(context){ 
	//remove custom positioning from 'troll-asset' class
	if(context == "solo-troll"){ 
		$(".troll-asset").removeClass(function (index, classNames) {
			return (classNames.match(/\bposition-([0-6])+/g) || []).join(' ');
		});
	} else if(context == "pod-troll"){ 
		$(".troll-asset").removeClass(function (index, classNames) {
			return (classNames.match(/\bposition-([7-8])+/g) || []).join(' ');
		});
	} else if(context == "preview-poster"){ 
		$("#preview-poster").removeClass(function (index, classNames) {
			return (classNames.match(/\bposition-([2])+/g) || []).join(' ');
		});		
	}
}

/* NAMING FUNCTIONS
-------------------------------------- */
/* List of names */
var names = [];

var girlNames = [
	['Queen', 'Princess', 'Miss', 'Lady', 'Wild', 'Spring', 'Winter', 'Summer', 'Autumn'],
	['Happy', 'Dazzle', 'Glitter', 'Sugar', 'Sunshine', 'Fluffy', 'Hugtime', 'Truth', 'Charming'],
	['Diamond', 'Sparkles', 'Friend', 'Flower', 'Rainbow', 'Artist', 'Spirit', 'Pixie']
];
var boyNames = [
	['King', 'Prince', 'Mister', 'Duke', 'Lord', 'Spring', 'Winter', 'Summer', 'Autumn'], 
	['Hugtime', 'Moon', 'River', 'Glitter', 'Sunshine', 'Leaf', 'Happy', 'Fuzzy', 'Daring'], 
	['Firefly', 'Robin', 'Racer', 'Friend', 'Artist', 'Spirit', 'Singer']
];

var girlNamesTall = ['Happy', 'Sugar', 'Fluffy', 'Hugtime'];
var boyNamesTall = ['Hugtime','Happy', 'Fuzzy'];

function whichNames(){ 
	if(PARTNUM.d == 1){ 
		names = girlNames;
	} else if(PARTNUM.d == 2){ 
		names = boyNames;
	}
}

/* Randomize names */
function randomizeNames() {
	whichNames();
	randomNum1 = Math.floor(Math.random() * names[0].length);
	randomNum2 = Math.floor(Math.random() * names[1].length);
	randomNum3 = Math.floor(Math.random() * names[2].length);

	NAME.name1 = names[0][randomNum1];
	NAME.name2 = names[1][randomNum2];
	NAME.name3 = names[2][randomNum3];
	
	PARTNUM.name1 = randomNum1 + 1;
	PARTNUM.name2 = randomNum2 + 1;
	PARTNUM.name3 = randomNum3 + 1;

	browserDisplayNames();
}

/* Move forward or backward in names array */
function moveThroughNames(e) {
	whichNames();
	var $direction = "";
	var which = 0; 
	var whichindex = 0;
	var currIndex, nextIndex, prevIndex; 

	var $nameInput = e.currentTarget.id;
	var userClickedOnName = /name-\d+/;

	// Check to see if user clicked on name instead of 
	// navigation arrow
	if($nameInput.match(userClickedOnName)){ 
		$direction = "increment";
		switch ($nameInput){ 
			case "name-1": 
			which = 1;
			break;
			case "name-2":
			which = 2;
			break;
			case "name-3": 
			which = 3;
			break; 
		}

	} else { 
		$direction = e.data.option;
		which = $(this).data('selection');
	} 
	
	whichindex = which - 1;
	// Get current of index of name in array names 
	for(var i = 0; i < names[whichindex].length; i++) {
		if(names[whichindex][i] == NAME["name" + which]) {
		  currIndex = i;
		}
	}	
	// Determine which direction to move towards the next name
	if ($direction == "increment"){
		moveTo = currIndex + 1;
		if(moveTo > (names[whichindex].length - 1)){ moveTo = 0; }
		PARTNUM["name"+which] = moveTo + 1;
		 
	} else if ($direction == "decrement"){
		moveTo = currIndex - 1;
		if(moveTo < 0){ moveTo = (names[whichindex].length - 1); }
		PARTNUM["name"+which] = moveTo + 1;

	}
	
	// Set names globally
	NAME["name" + which] = names[whichindex][moveTo];

	browserDisplayNames();
}

/* Output names to browser */
function browserDisplayNames() {
	var whichPosition = "";

	if(PARTNUM.d == 1){ 
		for (i = 0; i < girlNamesTall.length; i++){ 
			if(girlNamesTall[i] == NAME.name2){ 
				whichPosition = "a";
				break;
			} else { 
				whichPosition = "b";
			}
		}
	} else if (PARTNUM.d == 2){ 
		for (i = 0; i < boyNamesTall.length; i++){
			if(boyNamesTall[i] == NAME.name2){ 
				whichPosition = "a";
				break;
			} else { 
				whichPosition = "b";
			}
		}
	}

	// Store file paths for name images
	NAME.paths.a = "gender-"+ PARTNUM.d +"/names/first/name-"+ PARTNUM.name1; // First Name
	NAME.paths.b = "gender-"+ PARTNUM.d +"/names/second/name-"+ PARTNUM.name2; // Second Name
	NAME.paths.c = "gender-"+ PARTNUM.d +"/names/third/position-"+ whichPosition +"/name-"+ PARTNUM.name3; // Third Name

	// Display output
	$("#name-1").html(NAME.name1);
	$("#name-2").html(NAME.name2);
	$("#name-3").html(NAME.name3 == "" ? '(none)' : NAME.name3);

	$("#asset-display-zone #first, #preview-poster-first").attr("src", WEB_PREFIX + NAME.paths.a + WEB_SUFFIX);
	$("#asset-display-zone #second, #preview-poster-second").attr("src", WEB_PREFIX + NAME.paths.b + WEB_SUFFIX);
	$("#asset-display-zone #third, #preview-poster-third").attr("src", WEB_PREFIX + NAME.paths.c + WEB_SUFFIX);

	$("#coloring-page-first").attr("src", PRINT_PREFIX + NAME.paths.a + PRINT_SUFFIX);
	$("#coloring-page-second").attr("src", PRINT_PREFIX + NAME.paths.b + PRINT_SUFFIX);
	$("#coloring-page-third").attr("src", PRINT_PREFIX + NAME.paths.c + PRINT_SUFFIX);

	NAMESELECTED = true;

	//characterAssetLoader();
}

function characterAssetLoader(){ 
  // Do we need to show the loader?
  if(!LOADED) return;

  var parts = [
    "l_front", // pod 
    "j_back", // outfit back
    "h",   // hair style
    "i",   // hair accessory
    "m",   // pod accessory
    "e",   // pose/ troll body
    "f",   // expression
    "j_front", // outfit front
    "k",   // attire accessory
    "l_back", // pod leaves
    "a",   // first name
    "b",   // second name 
    "c",   // last name
  ];

  var newtoload = [];
  var i = parts.length;
  var assetToLoad = "";

  while(i--) {
    // If no path/ image to load then skip to next asset
    if
    ( parts[i] == "l_back"  ||
      parts[i] == "l_front" ||
      parts[i] == "m"
    ){ 
      if(POD[parts[i]] == undefined) continue;
      assetToLoad = WEB_PREFIX + POD[parts[i]] + WEB_SUFFIX;
    } 
    else if
    ( parts[i] == "a" ||
      parts[i] == "b" ||
      parts[i] == "c"
    ){ 
      if(NAME[parts[i]] == undefined) continue;
      assetToLoad = WEB_PREFIX + NAME["paths"][parts[i]] + WEB_SUFFIX;
    } 
    else 
    { 
      if(TROLL[parts[i]] == undefined) continue;
      assetToLoad = WEB_PREFIX + TROLL[parts[i]] + WEB_SUFFIX;
    }

    // Has this asset been loaded before?
    if(assets[assetToLoad]) {
      //image already loaded
      parts.splice(i, 1);

    } else {
      //image needs to be loaded and removed from the manifest
      var img = new Image();
      img.onload = registerAsset;
      img.onerror = preloadFailed;
      // Set image source 
      img.src = assetToLoad;
      newtoload.push(img.src);

    }

  }

  if(newtoload.length > 0) {
    requiredAssetsRemaining = newtoload;
    timeout = new Date();
    $("#stage").addClass("loading");
  }
}

// TIMING FUNCTION FOR REMOVING LOADING SCREEN 
// AFTER CHARACTER ASSETS HAVE BEEN LOADED
var requiredAssetsRemaining = [];
var timeout;

setInterval(function(){

	var i = requiredAssetsRemaining.length;

	while(i--) {
		if(assets[requiredAssetsRemaining[i]]) {
			requiredAssetsRemaining.splice(i, 1);
		}
	}

	var now = new Date();
	if(requiredAssetsRemaining.length == 0 || now - timeout > 10000) {
		$("#stage").removeClass("loading");
  }

}, 750);

function highlightSelectedGender(){ 
	if(!$("#girl").hasClass("selected") && !$("#boy").hasClass("selected")){
		setTimeout(function(){
			$("#girl a, #boy a, #step-forward > a.hit-box").css("pointer-events","auto");
		}, 5300);
		//User experience effect. Promote gender select
		setTimeout(function(){
			$("#girl .bg-display-item").css("background-image", "url('img/generic-troll/girl-hover.svg')");
			$("#girl .glow").css({"opacity": 1, "transform":"scale(1)"});
		}, 3500);
		setTimeout(function(){
			$("#girl .bg-display-item").css("background-image", "url('img/generic-troll/girl.svg')");
			$("#girl .glow").css({"opacity": 0, "transform":"scale(0.8)"});
		}, 4300);
		setTimeout(function(){
			$("#boy .bg-display-item").css("background-image", "url('img/generic-troll/boy-hover.svg')");
			$("#boy .glow").css({"opacity": 1, "transform":"scale(1)"});
		}, 4500);
		setTimeout(function(){
			$("#boy .bg-display-item").css("background-image", "url('img/generic-troll/boy.svg')");
			$("#boy .glow").css({"opacity": 0, "transform":"scale(0.8)"});
		}, 5300);

		// setTimeout(function(){ 
		// 	$("#girl .glow").css({"opacity": 1, "transform":"scale(1)"});
		// 	$("#boy .glow").css({"opacity": 0, "transform":"scale(0.8)"});
		// 	$("#girl .bg-display-item").css("background-image", "url(img/generic-troll/girl-hover.svg)");
		// 	$("#girl").addClass("selected");

		// }, 3700);
	} 
	if($("#girl").hasClass("selected") || $("#boy").hasClass("selected")){ 
		setTimeout(function(){
			$("#girl a, #boy a, #step-forward > a.hit-box").css("pointer-events","auto");
		}, 3000);
	}
	if ($("#girl").hasClass("selected")){ 
		setTimeout(function(){
			$("#girl .glow").css({"opacity": 1, "transform":"scale(1)"});
			$("#girl .bg-display-item").css("background-image", "url(img/generic-troll/girl-hover.svg)");
		}, 2600);	
	} else if ($("#boy").hasClass("selected")){ 
		setTimeout(function(){
			$("#boy .glow").css({"opacity": 1, "transform":"scale(1)"});
			$("#boy .bg-display-item").css("background-image", "url(img/generic-troll/boy-hover.svg)");
		}, 2600);
	}
}

function removeSelectedGenderHighlight(){ 
		$("#boy .glow").css({"opacity": 0, "transform":"scale(0.8)"});
		$("#girl .glow").css({"opacity": 0, "transform":"scale(0.8)"});
		$("#boy .bg-display-item").css("background-image", "url(img/generic-troll/boy.svg)");
		$("#girl .bg-display-item").css("background-image", "url(img/generic-troll/girl.svg)");
		$("#girl a, #boy a").css("pointer-events","none");
		document.getElementById("girl-image").style.WebkitAnimationName = "";
		document.getElementById("boy-image").style.animationName = "";
}