function loadTheseAssets(loadThese){ 
    var newtoload = [];
    var i = loadThese.length;
    var genderCount= 1;
    var alternate = loadThese.length;
    var breakloop = false;
    var gender = PARTNUM.d;

    if(PARTNUM.d == undefined){gender = 1;}

    while(i--) {
        var assetToLoad = ""; 
        assetToLoad = "assets/web/svg/gender-"+gender+"/"+loadThese[i]+".svg";
        // Has this asset been loaded before?
        if(assets[assetToLoad]) {
            //image already loaded
            parts.splice(i, 1);

        } else {
            //image needs to be loaded and removed from the manifest
            var img = new Image();
            img.onload = registerAsset;
            img.onerror = preloadFailed;
            // Set image source 
            img.src = assetToLoad;
            newtoload.push(img.src);
        }
        
        if(i == 0){
            i = alternate;
            breakloop = true;
            genderCount++;

            if(gender == 1){gender = 2;}
            else{gender = 1;}
        }

        if(genderCount == 3){ 
            if(breakloop == true){return;}
        }

    }
}

function Set1CharacterAssetLoading(){ 
// 7.5MB
  var theseAssets = [];
  var template = [
    "expression/expression-1",
    "expression/expression-2",
    "expression/expression-3",
    "expression/expression-4",
    "hair/style-1/style-1-acc-1",
    "hair/style-1/style-1-acc-2",
    "hair/style-1/style-1-acc-3",
    "hair/style-1/style-1-acc-4",
    "hair/style-1/style-1-color-1",
    "hair/style-1/style-1-color-2",
    "hair/style-1/style-1-color-3",
    "hair/style-1/style-1-color-4",
    "hair/style-2/style-2-acc-1",
    "hair/style-2/style-2-acc-2",
    "hair/style-2/style-2-acc-3",
    "hair/style-2/style-2-acc-4",
    "hair/style-2/style-2-color-1",
    "hair/style-2/style-2-color-2",
    "hair/style-2/style-2-color-3",
    "hair/style-2/style-2-color-4",
    "hair/style-3/style-3-acc-1",
    "hair/style-3/style-3-acc-2",
    "hair/style-3/style-3-acc-3",
    "hair/style-3/style-3-acc-4",
    "hair/style-3/style-3-color-1",
    "hair/style-3/style-3-color-2",
    "hair/style-4/style-4-color-3",
    "hair/style-4/style-4-color-4",
    "hair/style-4/style-4-acc-1",
    "hair/style-4/style-4-acc-2",
    "hair/style-4/style-4-acc-3",
    "hair/style-4/style-4-acc-4",
    "hair/style-4/style-4-color-1",
    "hair/style-4/style-4-color-2",
    "hair/style-4/style-4-color-3",
    "hair/style-4/style-4-color-4"
  ];

  var a = 0;
  for(i = 1; i < 5; i++){ 
    for(b = 0; b < template.length; b++){ 
        for(n = 0; n < 5; n++){ 
            theseAssets[a] = "troll/pose-"+i+"/"+ template[b];
            a++;
        }
    }
  }

  loadTheseAssets(theseAssets);
}

function Set2CharacterAssetLoading(){ 
// 7.5MB
  var theseAssets = [];
  var template = [
    "attire/attire-acc-1",
    "attire/attire-acc-2",
    "attire/attire-acc-3",
    "attire/attire-acc-4",
    "attire/outfit-1/outfit-a-1",
    "attire/outfit-1/outfit-b-1",
    "attire/outfit-2/outfit-a-2",
    "attire/outfit-2/outfit-b-2",
    "attire/outfit-3/outfit-a-3",
    "attire/outfit-3/outfit-b-3",
    "attire/outfit-4/outfit-a-4",
    "attire/outfit-4/outfit-b-4",
  ];

  var a = 0;
  for(i = 1; i < 5; i++){ 
    for(b = 0; b < template.length; b++){ 
        for(n = 0; n < 5; n++){ 
            theseAssets[a] = "troll/pose-"+i+"/"+ template[b];
            a++;
        }
    }
  }

  loadTheseAssets(theseAssets);
}

function podRelatedAssetLoading(){ 
// 2.4MB
var theseAssets = [];
var template = [
    "pods/pod-1/pod-1-acc-1",
    "pods/pod-1/pod-1-acc-2",
    "pods/pod-1/pod-1-acc-3",
    "pods/pod-1/pod-1-acc-4",
    "pods/pod-1/pod-1-pod-a",
    "pods/pod-1/pod-1-pod-b",

    "pods/pod-2/pod-2-acc-1",
    "pods/pod-2/pod-2-acc-2",
    "pods/pod-2/pod-2-acc-3",
    "pods/pod-2/pod-2-acc-4",
    "pods/pod-2/pod-2-pod-a",
    "pods/pod-2/pod-2-pod-b",

    "pods/pod-3/pod-3-acc-1",
    "pods/pod-3/pod-3-acc-2",
    "pods/pod-3/pod-3-acc-3",
    "pods/pod-3/pod-3-acc-4",
    "pods/pod-3/pod-3-pod-a",
    "pods/pod-3/pod-3-pod-b",

    "pods/pod-4/pod-4-acc-1",
    "pods/pod-4/pod-4-acc-2",
    "pods/pod-4/pod-4-acc-3",
    "pods/pod-4/pod-4-acc-4",
    "pods/pod-4/pod-4-pod-a",
    "pods/pod-4/pod-4-pod-b",
    ];

    for(i = 1; i < 3; i++){ 
        for(b = 0; b < template.length; b++){ 
            theseAssets[b] = template[b];
        }
    } 

    loadTheseAssets(theseAssets);
}

function nameRelatedAssetLoading(){ 
// 526kb
var theseAssets = [];
  var template = [    
    "names/first/name-1",
    "names/first/name-2",
    "names/first/name-3",
    "names/first/name-4",
    "names/first/name-5",
    "names/first/name-6",
    "names/first/name-7",
    "names/first/name-8",
    "names/first/name-9",

    "names/second/name-1",
    "names/second/name-2",
    "names/second/name-3",
    "names/second/name-4",
    "names/second/name-5",
    "names/second/name-6",
    "names/second/name-7",
    "names/second/name-8",
    "names/second/name-9",

    "names/third/position-a/name-1",
    "names/third/position-a/name-2",
    "names/third/position-a/name-3",
    "names/third/position-a/name-4",
    "names/third/position-a/name-5",
    "names/third/position-a/name-6",
    "names/third/position-a/name-7",
    "names/third/position-a/name-8",

    "names/third/position-b/name-1",
    "names/third/position-b/name-2",
    "names/third/position-b/name-3",
    "names/third/position-b/name-4",
    "names/third/position-b/name-5",
    "names/third/position-b/name-6",
    "names/third/position-b/name-7",
    "names/third/position-b/name-8",
    ];

    for(i = 1; i < 3; i++){ 
        for(b = 0; b < template.length; b++){ 
            theseAssets[b] = template[b];

            if( (i == 2 && theseAssets[b] == "names/third/position-a/name-8") ||
                (i == 2 && theseAssets[b] == "names/third/position-b/name-8")
            ){ 
                theseAssets.splice(b, 1);
            }
        }
    } 

    loadTheseAssets(theseAssets);
}

function auxillaryAssetLoading(){ 
    var theseAssets = [];
    var template = [

        "troll/pose-1/pose",
        "troll/pose-2/pose",
        "troll/pose-3/pose",
        "troll/pose-4/pose",
        "troll/pose-1/pose",
        "troll/pose-2/pose",
        "troll/pose-3/pose",
        "troll/pose-4/pose",

        "troll/pose-1/hair/style-0-color-1",
        "troll/pose-2/hair/style-0-color-1",
        "troll/pose-3/hair/style-0-color-1",
        "troll/pose-4/hair/style-0-color-1",
        "troll/pose-1/hair/style-0-color-1",
        "troll/pose-2/hair/style-0-color-1",
        "troll/pose-3/hair/style-0-color-1",
        "troll/pose-4/hair/style-0-color-1",

        "pods/pod-1/pod-1-pod-b",
        "pods/pod-1/pod-1-pod-a",
        "pods/pod-1/pod-1-pod-b",
        "pods/pod-1/pod-1-pod-a",

        "pods/pod-1/pod-1-acc-1",
        "pods/pod-1/pod-1-acc-1",

        "troll/pose-1/attire/attire-acc-1",
        "troll/pose-1/attire/outfit-1/outfit-a-1",
        "troll/pose-1/expression/expression-1",
        "troll/pose-1/hair/style-1/style-1-acc-1",
        "troll/pose-1/hair/style-1/style-1-color-1",
        "troll/pose-1/attire/outfit-1/outfit-b-1",

        "troll/pose-2/attire/attire-acc-1",
        "troll/pose-2/attire/outfit-1/outfit-a-1",
        "troll/pose-2/expression/expression-1",
        "troll/pose-2/hair/style-1/style-1-acc-1",
        "troll/pose-2/hair/style-1/style-1-color-1",
        "troll/pose-2/attire/outfit-1/outfit-b-1",

        "troll/pose-3/attire/attire-acc-1",
        "troll/pose-3/attire/outfit-1/outfit-a-1",
        "troll/pose-3/expression/expression-1",
        "troll/pose-3/hair/style-1/style-1-acc-1",
        "troll/pose-3/hair/style-1/style-1-color-1",
        "troll/pose-3/attire/outfit-1/outfit-b-1",

        "troll/pose-4/attire/attire-acc-1",
        "troll/pose-4/attire/outfit-1/outfit-a-1",
        "troll/pose-4/expression/expression-1",
        "troll/pose-4/hair/style-1/style-1-acc-1",
        "troll/pose-4/hair/style-1/style-1-color-1",
        "troll/pose-4/attire/outfit-1/outfit-b-1",

        "troll/pose-1/attire/attire-acc-1",
        "troll/pose-1/attire/outfit-1/outfit-a-1",
        "troll/pose-1/expression/expression-1",
        "troll/pose-1/hair/style-1/style-1-acc-1",
        "troll/pose-1/hair/style-1/style-1-color-1",
        "troll/pose-1/attire/outfit-1/outfit-b-1",

        "troll/pose-2/attire/attire-acc-1",
        "troll/pose-2/attire/outfit-1/outfit-a-1",
        "troll/pose-2/expression/expression-1",
        "troll/pose-2/hair/style-1/style-1-acc-1",
        "troll/pose-2/hair/style-1/style-1-color-1",
        "troll/pose-2/attire/outfit-1/outfit-b-1",

        "troll/pose-3/attire/attire-acc-1",
        "troll/pose-3/attire/outfit-1/outfit-a-1",
        "troll/pose-3/expression/expression-1",
        "troll/pose-3/hair/style-1/style-1-acc-1",
        "troll/pose-3/hair/style-1/style-1-color-1",
        "troll/pose-3/attire/outfit-1/outfit-b-1",

        "troll/pose-4/attire/attire-acc-1",
        "troll/pose-4/attire/outfit-1/outfit-a-1",
        "troll/pose-4/expression/expression-1",
        "troll/pose-4/hair/style-1/style-1-acc-1",
        "troll/pose-4/hair/style-1/style-1-color-1",
        "troll/pose-4/attire/outfit-1/outfit-b-1",
    ];

    for(i = 1; i < 3; i++){ 
        for(b = 0; b < template.length; b++){ 
            theseAssets[b] = template[b];
        }
    } 

    loadTheseAssets(theseAssets);
}