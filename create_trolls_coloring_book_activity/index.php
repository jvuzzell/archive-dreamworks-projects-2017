<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

    <title>Dreamworks Create Troll Friend</title>

    <style>html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{border:0;font-size:100%;font:inherit;vertical-align:baseline;margin:0;padding:0}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:none}table{border-collapse:collapse;border-spacing:0}</style>
    <style>
      body {
        text-align: center;
      }
      iframe {
        margin: 36px auto;
        width: 568px;
        height: 320px;
      }
      #mobile {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: url(beta/img/rotate-screen.jpg) center center no-repeat;
        background-size: cover;
      }
      @media only screen
      and (max-device-width : 1335px)
      and (orientation : portrait) {
        iframe.active {
          display: none;
        }
        #mobile {
          display: block;
        }
      }
      @media (min-width: 667px) {
        iframe, .mobile {
          width: 667px;
          height: 375px;
        }
      }
      @media (min-width: 736px) {
        iframe, .mobile {
          width: 736px;
          height: 414px;
        }
      }
      @media (min-width: 1335px) {
        iframe, .mobile {
            width: 1024px;
            height: 578px;
        }
      }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script type="text/javascript">
      $(window).load(function(){ $("iframe").addClass('active'); });
    </script>
  </head>

  <body>

    <iframe id="game" src="dev/index.html"></iframe>
    <div id="mobile"></div>

  </body>
</html>
