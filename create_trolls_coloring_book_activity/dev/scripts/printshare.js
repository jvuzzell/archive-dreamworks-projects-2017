var doc,
	poster_image, // PDF - Poster Image
	coloring_image, // PDF - Coloring Page Image
	share_gallery_poster, // User Download Page & Share Gallery Poster

	pdfready = false,
	shareready = false,

	w = 2550,
	h = 3300,

	lores_w = 1080,
	lores_h = 1398,

	asset_format  = "png",
	asset_quality = "",
	reqCount = 0,

	posterLayers  = {}
	
//iOS hack fix
try {
	if(
	/iPad|iPhone|iPod/i.test(navigator.userAgent) && 
	!window.MSStream 

	// &&
	// !/os 9_/i.test(navigator.userAgent) && 
	// !/os 10_/i.test(navigator.userAgent) && 
	// !/os 11_/i.test(navigator.userAgent) && 
	// !/os 12_/i.test(navigator.userAgent)

	) {
		w = 1750;
		h = 2264;
		// Asset quality dictated by which manifests loaded in <head> of index.html
		asset_quality = 'lores';
	}
} catch(e) {}

//////////////////////////////////////////////////////
//
// START HERE -- LISTENING FOR USER CLICKS
//
//////////////////////////////////////////////////////

$(document).ready(function(){
	$("#generate-pdf").on("click", function(){kickoffAssetGeneration(); delayNavigation("exit",0);}); 

	$("#final-step-download").on("click", function(){ $("#shade, #download").toggleClass("open"); });
	$("#return-to-start").on("click", function(){ $("#shade, #restart").toggleClass("open"); });
	$("div.popup#download a.close").on("click", function(){ $("#shade, div.popup#download").toggleClass("open"); });
	$("#restart-yes, #restart-no, #restart a.close").on("click", function(){ $("#shade, div.popup#restart").toggleClass("open"); });
	$("#restart-yes").on("click", returnToStart);
		
	$("#final-step-print").on("click", function(){
		if(doc) {
		  var fn = NAME.name1.replace(' ', '').toLowerCase() + '-' + NAME.name2.replace(' ', '').toLowerCase()  + '-' + NAME.name3.replace(' ', '').toLowerCase() +'.pdf';
		  doc.save(fn);
		}
	});

	/* Poster Previewing
	----------------------------- */
	$("#show-poster").on("click", function(){
		$("#shade").addClass("open"); 
		$("#poster-preview-enlarge").addClass("open");
		$("#poster-preview-enlarge").html("<a class='close'>+</a><img class='preview-image' src='"+poster_image+"'/>");
		
	});

	$("#show-coloring-page").on("click", function(){
		$("#shade").addClass("open"); 
		$("#coloring-preview-enlarge").addClass("open");
		$("#coloring-preview-enlarge").html("<div class='bg-img coloring-page-stamp'></div><a class='close'>+</a><img class='preview-image' src='"+coloring_image+"'/>");
		
	});

	$("#poster-preview-enlarge, #coloring-preview-enlarge").on("click", function(){
		$("#shade, #poster-preview-enlarge, #coloring-preview-enlarge").removeClass("open"); 

	});

	/*---------------------------*/

});

function kickoffAssetGeneration(){ 
	//this dispatch function helps alliviate the noticiability of animation freezes
	//during asset/pdf generation
	
	//functionally it doesn't do anything except make sure the animation is visible and running before
	//doing the heavy lifting
	
	//since the animation is a step/discrete function it's less noticiable than
	//a smooth moving progress bar or spinner
	
	$("#final-step-pre").hide();
	$("#final-step-loading").show();
	$("#final-step-loading > .spinner").css("display", "block");
	
	setTimeout(preloadAssetImages, 2000);
}

function assetFailed() {
	//simply try again here, and then give up
	console.log("Failed to load: ");
	console.log(this);
	var img = new Image();
	img.onload = this.onload;

	//be careful uncommenting this line- it will repeatedly try to fetch failed images
	//this will be an infinite loop for most errors (404 are the most obvious)
	//img.onerror = this.onerror;

	img.onerror = function() {
	console.log("Twice, failed to load: \n");
	console.log(this);
	
	//uncomment this section to prevent PDFs from generating with missing assets

	//alert("We're sorry, we had trouble creating your Dinotrux.");
	//$("#final-step-loading").hide();
	//$("#final-step-pre").show();

	//recomment this line if you uncomment the section above
	checkLoadedAndGenerateAssets();
	}

	img.src = this.src;
}

function preloadAssetImages() {
	doc = null,
	poster_image = null,
	share_gallery_poster = null,
	
	pdfready = false,
	shareready = false,
	
	reqCount = 0;

	/* PRELOAD IMAGES FOR POSTER
	------------------------------------ */ 
	// STEP 1 - 
	// Set keys in order that elements will layer on our poster from Back to Front
	posterLayers = {
		"background": null,
		"l_front" : null, // pod 
		"j_back" : null, // outfit back
		"m" : null, 	 // pod accessory
		"e" : null, 	 // pose/ troll body
		"f" : null, 	 // expression
		"j_front" : null, // outfit front
		"k" : null, 	 // attire accessory
		"h" : null, 	 // hair style
		"i" : null, 	 // hair accessory
		"l_back" : null, // pod leaves
		"a" : null, 	 // first name
		"b" : null, 	 // second name 
		"c" : null, 	 // last name
		"frame": null
	}
	coloringPageLayers = {
		"background": null,
		"l_front" : null, // pod 
		"j_back" : null, // outfit back
		"m" : null, 	 // pod accessory
		"e" : null, 	 // pose/ troll body
		"f" : null, 	 // expression
		"j_front" : null, // outfit front
		"k" : null, 	 // attire accessory
		"h" : null, 	 // hair style
		"i" : null, 	 // hair accessory
		"l_back" : null, // pod leaves
		"a" : null, 	 // first name
		"b" : null, 	 // second name 
		"c" : null, 	 // last name
		"frame": null
	}

	// STEP 2 - 
	// Convert stored filepaths to images elements
	// This function processes two images at one time. 
	// src = poster image
	// csrc = coloring page image
	$.each(posterLayers, function(index, element, array) {  

		// Build image source path
		var src = "";
		var csrc = "";
		if (asset_quality == "lores"){src = "assets/web/png-lores/"; csrc = "assets/print/png-lores/";} 
		else {src = "assets/web/png/"; csrc = "assets/print/png/";}
		
		if
		(	index == "l_back"  ||
			index == "l_front" ||
			index == "m"
		){ 
			if(POD[index] == "") return;
			src += POD[index];
			csrc += POD[index];
		} 
		else if
		(	index == "a" ||
			index == "b" ||
			index == "c"
		){ 
			if(NAME["paths"][index] == "") return;
			src += NAME["paths"][index];
			csrc += NAME["paths"][index];
		} 
		else if 
		(
			index == "background" ||
			index == "frame"
		){ 
			src = "assets/poster/";
			csrc = "assets/poster/";
			if(asset_quality == "lores"){src = "assets/poster/lores/"; csrc = "assets/poster/lores/";}
			if (index == "background"){src += "poster-frame-b"; csrc += "coloring-frame-b";}
			if (index == "frame"){src += "poster-frame-a"; csrc += "coloring-frame-a";}
		}
		else 
		{ 
			if(TROLL[index] == "") return;
			src += TROLL[index];
			csrc += TROLL[index];
		}
		
		if(asset_quality == "lores"){
			src += "_lores.png";
			csrc += "_lores.png";
		} else { 
			src += ".png";
			csrc += ".png";
		}
				
		// Load image for poster
		var img = new Image();	
		img.onload = checkLoadedAndGenerateAssets; 
		img.onerror = assetFailed;
		img.src = src; 
		posterLayers[index] = img;
		reqCount++;	

		// Load image for coloring page
		var cimg = new Image();	
		cimg.onload = checkLoadedAndGenerateAssets; 
		cimg.onerror = assetFailed;
		cimg.src = csrc; 
		coloringPageLayers[index] = cimg;
		reqCount++;	
		
	});

	/* ----------------------------------- */
} // end preLoadAssetImages(

///////////////////////////////////////////////////////////////
//
// MAIN FUNCTION TO BUILD CREATE IMAGES, SHARE TO GALLERY,
// AND CREATE PDF.  RUNS AFTER ALL IMAGES ARE LOADED
//
///////////////////////////////////////////////////////////////

function checkLoadedAndGenerateAssets() { 
	reqCount--;
	
	if(reqCount == 0) {
		
		/* PDF - POSTER IMAGE
		-------------------------------------------------------- */
		var poster_canvas = document.createElement('canvas');
		poster_canvas.width = w;
		poster_canvas.height = h;

		var poster_context = poster_canvas.getContext('2d');
		poster_context.fillStyle = "white";
		poster_context.fillRect(0, 0, w, h);

		$.each(posterLayers, function(index, element, array) { 
			//make sure it's not an image that errored
			if(!element || element.width == 0){
			// skip to next key in posterLayers
			return true;
			}

			// custom positioning based on the user selected pod
			if (index == "background" || index == "frame"){ 
				poster_context.drawImage(element, 0, 0, w, h);
			} else { 
				positionTrollAssets("poster_context", index, element, array);
			}
		});

		// Store image data then free resources
		poster_image = poster_canvas.toDataURL('image/jpeg', .90);
		poster_canvas.remove();

		/* PDF - COLORING PAGE IMAGE
		-------------------------------------------------------- */
		var coloring_canvas = document.createElement('canvas');
		coloring_canvas.width = w;
		coloring_canvas.height = h;

		var coloring_context = coloring_canvas.getContext('2d');
		coloring_context.fillStyle = "white";
		coloring_context.fillRect(0, 0, w, h);

		$.each(coloringPageLayers, function(index, element, array) { 
			//make sure it's not an image that errored
			if(!element || element.width == 0){
			// skip to next key in posterLayers
			return true;
			}
			
			// custom positioning based on the user selected pod
			if (index == "background" || index == "frame"){ 
				coloring_context.drawImage(element, 0, 0, w, h);
			} else { 
				positionTrollAssets("coloring_context", index, element, array);
			}


		});

		// Store image data then free resources
		coloring_image = coloring_canvas.toDataURL('image/jpeg', .90);
		coloring_canvas.remove();

		
		/* User Download Page & 
		//   Dreamworks Share Gallery Poster
		-------------------------------------------------------- */
		var share_gallery_poster_canvas = document.createElement('canvas');
		share_gallery_poster_canvas.width = lores_w;
		share_gallery_poster_canvas.height = lores_h;

		var share_gallery_poster_context = share_gallery_poster_canvas.getContext('2d');
		share_gallery_poster_context.fillStyle = "white";
		share_gallery_poster_context.fillRect(0, 0, lores_w, lores_h);

		$.each(posterLayers, function(index, element, array) { 
			//make sure it's not an image that errored
			if(!element || element.width == 0){
			// skip to next key in posterLayers
			return true;
			}

			// custom positioning based on the user selected pod
			if (index == "background" || index == "frame"){ 
				share_gallery_poster_context.drawImage(element, 0, 0, lores_w, lores_h);
			} else { 
				positionTrollAssets("share_gallery_poster_context", index, element, array);
			}

			share_gallery_poster = share_gallery_poster_canvas.toDataURL('image/jpeg', .90);
			share_gallery_poster_canvas.remove();

		});

		/* UPLOAD TO FAMILY GALLERY
		-------------------------------------------------------- */

		var title = escape(NAME.name1 + NAME.name2 + NAME.name3);
		var desc = escape('My TROLL FRIEND');

		var urlSplit = share_gallery_poster.split(',');
		var dataType = urlSplit[0];
		var base64Data = urlSplit[1];

		var domain = "";
		if (location.hostname == "localhost" || 
			location.hostname == "trolls.missionmedia.net"
		){ 
			domain = "http://dev.create.dreamworks.com";
		} 

		$.post(domain + "/upload/share_image", {
			"title": title,
			"description": desc,
			"activity-key": 'trolls',
			"image": urlSplit[1],
			"mime-type": "image/jpeg"
			}, function(r) {
				if(r.status != "success") {
					console.log("Image upload failed: " + r.reason + " - " + r.message);
				} else {
					//download
					$("#download .image").html('<img src="' + r.response.url + '" />');

					//share
					token = r.response.token;
					$("#final-step-share").data("token", token);
					$("#final-step-share").on("click", function(){
						if($(this).data('token')) {
							window.open(domain + "/shares/verify_upload?token=" + $(this).data('token'));
						}
					});

					shareready = true;
					updateFinalStepUI();
				}
		}).fail(function(d){
			console.log("Share post failed...");
			console.log(d);
			shareready = true;
			updateFinalStepUI();
		});

		/* CREATE PDF
		-------------------------------------------------------- */
		
		try {

			doc = new jsPDF('p', 'in', [8.5, 11]);

			/* POSTER IMAGE
			----------------------------------------- */
			doc.addImage(poster_image, 'JPEG', 0, 0, 8.5, 11);
			
			/* COLORING PAGE
			----------------------------------------- */
			doc.addPage();
			doc.addImage(coloring_image, 'JPEG', 0, 0, 8.5, 11);
			
			pdfready = true;
			updateFinalStepUI();

		} catch(e) {
			alert("We're sorry, we had trouble building your Troll Friend.");
			$("#final-step-loading").hide();
			$("#final-step-pre").show();
			console.log(e);
		}
	}

	function positionTrollAssets(context, index, element, array){ 
		if(
			index == "j_back" ||
			index == "e" ||
			index == "h" ||
			index == "i" ||
			index == "f" ||
			index == "j_front" ||
			index == "k"
		){ 	
			if(PARTNUM.l == 1){ 
	 			drawInContext(context, index, element, -105, -45);
			} else if(PARTNUM.l == 2){ 
				drawInContext(context, index, element, -145, -60);
			} else if(PARTNUM.l == 3){
				drawInContext(context, index, element, -130, -45);
			} else if(PARTNUM.l == 4){
				drawInContext(context, index, element, -112, -45);
			}				
			
		} else {
			positionNotTrollAssets(PARTNUM.l, context, index, element);						
		}
	}

	function positionNotTrollAssets(podNum, imageContext, assetName, whichElement){ 
		if(podNum == 1){ 
			drawInContext(imageContext, assetName, whichElement, 0, -45);
		} else if(podNum == 2){ 
			drawInContext(imageContext, assetName, whichElement, -55, -60);
		} else if(podNum == 3){ 
			drawInContext(imageContext, assetName, whichElement, -25, -45);
		} else if(podNum == 4){ 
			drawInContext(imageContext, assetName, whichElement, -8, -45);
		}
	}

	function drawInContext(imageContext, assetName, thatElement, xC, yC){ 
		if(imageContext == "poster_context"){ 
			poster_context.drawImage(thatElement, xC, yC, w, h);

		} else if(imageContext == "coloring_context"){ 
			coloring_context.drawImage(thatElement, xC, yC, w, h);

		} else if(imageContext == "share_gallery_poster_context"){ 
			share_gallery_positioning(imageContext, assetName, thatElement, xC, yC);

		}
	}
	
	function share_gallery_positioning(imageContext, assetName, thatElement, xC, yC){ 
		if(
			assetName == "j_back" ||
			assetName == "e" ||
			assetName == "h" ||
			assetName == "i" ||
			assetName == "f" ||
			assetName == "j_front" ||
			assetName == "k"
		){ 
			trollAsset = true;
		} else { 
			trollAsset = false;
		}
					
		if(PARTNUM.l == 1){ 
			if(trollAsset === true){ 
				xC = -35;
			} else { 
				xC = xC;
			}
				 		
		} else if(PARTNUM.l == 2){ 
			if(trollAsset === true){
				xC = -75;
			} else { 
				xC = -25;
			}
			
		} else if(PARTNUM.l == 3){
			if(trollAsset === true){ 
				xC = -60;
			} else { 
				xC = -15;
			}			
		} else if(PARTNUM.l == 4){
			if(trollAsset === true){ 
				xC = -42;
			} else { 
				xC = 0;
			}	
		}
		
		share_gallery_poster_context.drawImage(thatElement, xC, yC, lores_w, lores_h);
	}
}

//////////////////////////////////////////////////////
//
// FINISHED -- USER ABLE TO DOWNLOAD PDF
//
//////////////////////////////////////////////////////

function updateFinalStepUI() {
	if(pdfready) {

		generatePDFslideExit();
		setSlideTransitionDelay("next");
		
		// if(!mute) sfx_scream.play();
	}
}