
// UPLOADED PHOTOS
var ORIGINAL_PHOTO = {
  IMAGE : function(photoID, imgObj, w, h, t, s, n){ 
      this.id = photoID;
	  this.fileName = n;
	  this.width = w; 
	  this.height = h;
	  this.fileExtension = t; 
	  this.fileSize = s;
	  this.imgObj = imgObj;
  },
  
  GALLERY : {}, // WHERE UPLOADS ARE STORED 
  
  UPLOAD : function(file, photoID, pageID){ 
	  // THIS... 
      // 1) ...PROCESSES IMAGE 
      // 2) ...DISPLAYS IMAGE IN BROWSER
      // 3) ...STORES IMAGE SRC IN A GLOBAL VARIABLE
      var reader = new FileReader(),
      	  image  = new Image();

      reader.readAsDataURL(file); 
      reader.onload = function(_file) {
          image.src    = _file.target.result;              // url.createObjectURL(file);
          
          image.onload = function() {
              var w = this.width,
                  h = this.height,
                  t = file.type,                           // ext only: // file.type.split('/')[1],
                  n = file.name,
                  s = ~~(file.size/1024) +'KB',
                  size = ~~(file.size/1024);
                  
              if(w > 10000 || h > 10000 || size > 4200){ 
              	/** FAIL: FILE NOT UPLOADED TO BROWSER **/
              
                //document.getElementById("preview").innerHTML = 'Please keep uploads 500x500px and less than 500kb.';
                alert('Oops! Looks like your picture is too big! \nImages should be smaller than 4 MB.');
                var empty = ""; 
                //document.getElementById("fileToUpload").value(empty);
					
                return false;
              } else {  
              
              	/** SUCCESS: FILE UPLOADED TO BROWSER **/
				
				// 1) Displays to browser in preview area (or photo library)					
				image.setAttribute("id", "gallery_photo_"+photoID); 
				image.setAttribute("class", "preview_img");
				image.setAttribute("alt", "your image");
				
                // 2) Capture image and image data
				ORIGINAL_PHOTO["GALLERY"][photoID] = new ORIGINAL_PHOTO.IMAGE(photoID, image, w, h, t, s, n);
			
				
                // 3) Display dimensions, file size, and name associated with uploaded file
                SCRAPBOOK.PREP_PHOTO(photoID, pageID, undefined); // Draw image to canvas
       
              }// end else 
          };
          image.onerror = function() {
             /*  alert('Invalid file type: '+ file.type); */
             alert("Uh oh, we can't read that file type!");
              return false;
          };      
      };
  }
};/* END FILE UPLOAD */ 


// SCRAPBOOK 
var SCRAPBOOK = {
	  PDF : new jsPDF('p', 'in', [8.5, 11]), 
	  DOWNLOAD : undefined,
	  ASSET : function (pageID, imgObj){ 
  	  	this.pageID = pageID;
  	  	this.imgObj = imgObj;
	  },
	 
	  PAGE : function (pageID, themeID, titleID){ 
		  this.id = pageID; 
		  this.photoIDs = [];
		  this.theme = themeID;
		  this.title = titleID;
		  this.composite; // theme and photo ids
		  this.imgObj; // final image
		  this.registered = false;
	  },
	 
	  PHOTO : function(photoID, pageID, n, x, y, w , h, src, imgObj, degree){ 
	  	  this.pageID = pageID;
	  	  this.id = photoID;
		  this.fileName = n;
		  this.width = w;
		  this.height = h;  
		  this.src = src;
		  this.x = x;
		  this.y = y;
		  this.imgObj = imgObj;
		  this.rotation = degree;
	  },
	  
	  THEME : function(themeID, pageID, source){ 
	  	  this.themeID = themeID;
	  	  this.pageID = pageID; 
	  	  this.src = source;  
	  },
	  
	  TITLE : function(titleID, pageID, source){ 
	  	  this.titleID = titleID;
	  	  this.pageID = pageID; 
	  	  this.src = source;  		  
	  },
	  
	  ASSETS : {}, // STORE ALLL ASSETS RELATED TO THE SCRAPBOOK object
	  PAGES  : {}, // STORE SCRAPBOOK.PAGE 
	  PHOTOS : {}, // STORE SCRAPBOOK.PHOTO and theme images
	  THEMES : {}, // STORE SCRAPBOOK.THEME
	  TITLES : {},
	  
	  INIT : function (){ 
		  SCRAPBOOK["PAGES"]["page_0"] = new SCRAPBOOK.PAGE(0, 1);
	  	  SCRAPBOOK["PAGES"]["page_1"] = new SCRAPBOOK.PAGE(1, 1);
	  	  SCRAPBOOK["PAGES"]["page_2"] = new SCRAPBOOK.PAGE(2, 1);
	  	  SCRAPBOOK["PAGES"]["page_3"] = new SCRAPBOOK.PAGE(3, 1);
	  	  SCRAPBOOK["PAGES"]["page_4"] = new SCRAPBOOK.PAGE(4, 1);
	  	  SCRAPBOOK["PAGES"]["page_5"] = new SCRAPBOOK.PAGE(5, 1);
	  	  SCRAPBOOK["PAGES"]["page_6"] = new SCRAPBOOK.PAGE(6, 1);
	  	  SCRAPBOOK["PAGES"]["page_7"] = new SCRAPBOOK.PAGE(7, 1);
	  	  
	  	  var pageCount = 0;
	  	  for(page in SCRAPBOOK.PAGES){ 
	  	  	  if(SCRAPBOOK["PAGES"].hasOwnProperty(page)){ 
		  	  	  ++pageCount;
	  	  	  }
	  	  }
	  	  
	  	  for(i = 0; i < pageCount; i++){ 
		  	SCRAPBOOK.SET_THEME(i, 1, "theme"); 
	  	  }
  	  
	  }, 

	  SET_THEME : function(pageID, assetID, assetType){
	  	  // Sometimes we pass 'title' to assetType
	  	  if(assetType === undefined){assetType = "theme"};
	  	  
	  	  var src = "assets/page-"+pageID+"_"+assetType+"-"+assetID+".png";
	  	  if(assetType === "theme"){ 
	  	  	  SCRAPBOOK["PAGES"]["page_"+pageID]["theme"] = assetID;
		  	  SCRAPBOOK.THEMES["page-"+pageID+"_"+assetType+"-"+assetID] = new SCRAPBOOK.THEME(assetID, pageID, src);
	  	  } else if (assetType === "title"){ 
	  	  	  SCRAPBOOK["PAGES"]["page_"+pageID]["title"] = assetID; 
		  	  SCRAPBOOK.TITLES["page-"+pageID+"_"+assetType+"-"+assetID] = new SCRAPBOOK.TITLE(assetID, pageID, src);
		  	 
	  	  }
	  	  SCRAPBOOK.LOAD_PAGE(pageID);
  	  },
  	  
  	  LOAD_PAGE : function(pageNum){ 
	  	 var page = SCRAPBOOK["PAGES"]["page_"+pageNum],
	  	 	 numOfPhotos = page["photoIDs"].length,
	  	     images_to_load = numOfPhotos + 1, //added 1 to account for theme image
	  		 loadThese = {},
	  		 pageCompositeData = [];
	  		 
		 // PUSH ANY THEMES AND/ OR TITLES ASSOCIATED WITH THE PAGE TO THE LOADER
		 if(pageNum === 0){ 
		 	if(page.title !== undefined){ 
				images_to_load = images_to_load + 1; 
				loadThese["page-"+page.id+"_title-"+page.title] = SCRAPBOOK.TITLES["page-"+page.id+"_title-"+page.title]["src"];
				loadThese["page-"+page.id+"_theme-"+page.theme] = SCRAPBOOK.THEMES["page-"+page.id+"_theme-"+page.theme]["src"];
				
		 	} else { 
			 	loadThese["page-"+page.id+"_theme-"+page.theme] = SCRAPBOOK.THEMES["page-"+page.id+"_theme-"+page.theme]["src"];
		 	}
		 } else { 
			loadThese["page-"+page.id+"_theme-"+page.theme] = SCRAPBOOK.THEMES["page-"+page.id+"_theme-"+page.theme]["src"]; 
		 }
		 
		 // PUSH ANY PHOTOS ASSOCIATED WITH THE PAGE TO THE LOADER
		 for(var i=0; i < numOfPhotos; i++){ 
			 loadThese[page["photoIDs"][i]] = SCRAPBOOK["PHOTOS"][page["photoIDs"][i]]["src"];
		 }	
		 
		 for(id in loadThese){ 
		 	 var src = loadThese[id];
		 	 pageCompositeData.push(id);
		 	 
			 if(id in SCRAPBOOK.ASSETS){ 			 
				 if(loadThese[id] == SCRAPBOOK.ASSETS[id]["imgObj"]["src"]){ 
					// Prevent duplicate asset
					delete SCRAPBOOK.ASSETS[id];
					loadImage(id, loadThese[id]);	
				 } else { 
				 	loadImage(id, loadThese[id]);	 
				 }
				 
			 } else {
				loadImage(id, loadThese[id]);
			 }
			 
			function loadImage(id, imgSrc){ 
				 var imgObj = new Image();

				 imgObj.setAttribute("id", id); 
				 imgObj.onload = function(){ 
				 	SCRAPBOOK["ASSETS"][imgObj.id] = new SCRAPBOOK.ASSET(pageNum, imgObj);
					SCRAPBOOK.REGISTER_ASSETS(pageNum);
				 }
				 imgObj.src = imgSrc;
			 }	 		 
		 }	 
		 
		 SCRAPBOOK["PAGES"]["page_"+pageNum]["composite"] = pageCompositeData; //theme and photo ids
     },	
     
  	 REGISTER_ASSETS : function(pageNum){ 
	  	 var page = SCRAPBOOK["PAGES"]["page_"+pageNum]; 
	  	 page.registered = false;
	  	 
	  	 page.registered = SCRAPBOOK.SCRAPS(pageNum);
	  	 
	  	 if(page.registered === true){ 
		  	 SCRAPBOOK.DRAW_PAGE(pageNum);
		  	 
	  	 } else { 
		  	 console.log("Registering assets on page "+ pageNum +" failed.");
	  	 }
	  },	
	  
	 SCRAPS : function(pageNum){  
	  	 var page = SCRAPBOOK["PAGES"]["page_"+pageNum], 
	  	 	 scraps_registered = false;
	     var i = 0; // asset counter; all assets must be loaded before this function returns a value
	  	 
	  	 page.composite.forEach(function(compItem){ 
		  	i++; 
		  	scraps_registered = false;

		  	for(scrap in SCRAPBOOK.ASSETS){ 
			  	if(compItem === scrap){ 
			  		scraps_registered = true;
			  		i--;
			  		break;
			  	}
		  	}
	  	 });

	  	 if(i === 0){ 
		  	return(scraps_registered); 
	  	 }
	 },

	 DRAW_PAGE : function(pageNum){ 
	  	 var page = SCRAPBOOK["PAGES"]["page_"+pageNum],
	  	 	 canvas = document.createElement("canvas"),
			 context = canvas.getContext('2d'),
			 theme = SCRAPBOOK["ASSETS"]["page-"+page.id+"_theme-"+page.theme]["imgObj"];
			 
	     canvas.width = 820;
	     canvas.height = 1259;
		 clearCanvas(canvas, context);	 
		 
		 // Layer-1: Draw Photos
		 for(i=0; i < page.photoIDs.length; i++){ 
			 
			 var photoID = page["photoIDs"][i], 
			 	 dest = SCRAPBOOK.LAYOUT(photoID);

			 var img = SCRAPBOOK["ASSETS"][photoID]["imgObj"];
			 
			 context.rotate(dest.degree);
			 context.drawImage(img, dest.x, dest.y, dest.w, dest.h);
			 
			 // Canvas Clean
			 context.setTransform(1, 0, 0, 1, 0, 0);
			 
		 } 
		 
		 // Layer-2: Draw Theme 
		 context.drawImage(theme, 0, 0, 820, 1259);
		 
		 // Layer-3: Draw Title
		 if(pageNum === 0 && page.title !== undefined){
		  	var title = SCRAPBOOK["ASSETS"]["page-"+page.id+"_title-"+page.title]["imgObj"];

		  	context.drawImage(title, 0, 0, 820, 1259);	
		 }

		
		 // Convert canvas to source code for image tag. 
		 var pageImageSource = canvas.toDataURL(), 
		 	 scrapbook_page = new Image();

		 scrapbook_page.setAttribute("id","scrapbook_page_"+pageNum); 
		
		 scrapbook_page.src = pageImageSource;
		 scrapbook_page.onload = function(){
		 	// Store image object to associated page then display
		 	SCRAPBOOK["PAGES"]["page_"+pageNum]["imgObj"] = scrapbook_page; 	 
			SCRAPBOOK.DISPLAY(pageNum);	
		 }
	 
		// Free resources
		canvas.remove();
	 },
	 
	 DISPLAY : function(pageNum){ 
		
		var displayDest = document.getElementById("page-"+pageNum).getElementsByTagName("img")[0];
		var displayDestParent = document.getElementById("page-"+pageNum);
		var scrapbook_page = SCRAPBOOK["PAGES"]["page_"+pageNum]["imgObj"];

		displayDestParent.replaceChild(scrapbook_page, displayDest);
	 },
	 
	 LAYOUT : function(photoID){ 
	  	 var dest = {};
	  	 switch(photoID){ 
	  	 	 // FRONT COVER
		  	 case "photo_0":
		  	 	dest.x = 300; 
		  	 	dest.y = 345;
		  	 	dest.w = 405;
		  	 	dest.h = 405;
		  	 	dest.degree = 6.65*Math.PI/180;
		  	 break; 
		  	 // PAGE 1
		  	 case "photo_1":  
		  	 	dest.x = 80; 
		  	 	dest.y = 115;
		  	 	dest.w = 430;
		  	 	dest.h = 430;
		  	 	dest.degree = -6.25*Math.PI/180;		  	 	
		  	 break; 
		  	 case "photo_2": 
		  	 	dest.x = 465; 
		  	 	dest.y = 465;
		  	 	dest.w = 450;
		  	 	dest.h = 450;
		  	 	dest.degree = 10.25*Math.PI/180;		  	 
		  	 break;
		  	 // PAGE 2 
		  	 case "photo_3": 
		  	 	dest.x = 260; 
		  	 	dest.y = 170;
		  	 	dest.w = 620;
		  	 	dest.h = 620;
		  	 	dest.degree = 10.25*Math.PI/180;	
		  	 break;
		  	 // PAGE 3 
		  	 case "photo_4": 
		  	 	dest.x = -50; 
		  	 	dest.y = 105;
		  	 	dest.w = 332;
		  	 	dest.h = 332;
		  	 	dest.degree = -9.75*Math.PI/180;	
		  	 break; 
		  	 case "photo_5": 
		  	 	dest.x = 440; 
		  	 	dest.y = 65;
		  	 	dest.w = 430;
		  	 	dest.h = 430;
		  	 	dest.degree = 10.25*Math.PI/180;
		  	 break; 
		  	 case "photo_6": 
		  	 	dest.x = -180; 
		  	 	dest.y = 770;
		  	 	dest.w = 320;
		  	 	dest.h = 320;
		  	 	dest.degree = -12*Math.PI/180;
		  	 break; 
		  	 // PAGE 4
		  	 case "photo_7": 
		  	 	dest.x = 90; 
		  	 	dest.y = 90;
		  	 	dest.w = 405;
		  	 	dest.h = 405;
		  	 	dest.degree = -3*Math.PI/180;		
		  	 break; 
		  	 case "photo_8": 
		  	 	dest.x = 485; 
		  	 	dest.y = 440;
		  	 	dest.w = 440;
		  	 	dest.h = 440;
		  	 	dest.degree = 9*Math.PI/180;
		  	 break;
		  	 // PAGE 5
		  	 case "photo_9": 
		  	 	dest.x = 65; 
		  	 	dest.y = 142;
		  	 	dest.w = 530;
		  	 	dest.h = 530;
		  	 	dest.degree = -6.5*Math.PI/180;
		  	 break;
		  	 // PAGE 6
		  	 case "photo_10": 
		  	 	dest.x = 34; 
		  	 	dest.y = 130;
		  	 	dest.w = 340;
		  	 	dest.h = 340;
		  	 	dest.degree = -9.75*Math.PI/180;	
		  	 break; 
		  	 case "photo_11": 
		  	 	dest.x = 510; 
		  	 	dest.y = 110;
		  	 	dest.w = 350;
		  	 	dest.h = 350;
		  	 	dest.degree = 8.5*Math.PI/180;
		  	 break; 
		  	 case "photo_12": 
		  	 	dest.x = -42; 
		  	 	dest.y = 773;
		  	 	dest.w = 422;
		  	 	dest.h = 422;
		  	 	dest.degree = -9*Math.PI/180;
		  	 break; 
		  	 // PAGE 7
		  	 case "photo_13": 
		  	 	dest.x = 147; 
		  	 	dest.y = 188;
		  	 	dest.w = 300;
		  	 	dest.h = 300;
		  	 	dest.degree = -6.25*Math.PI/180;	
		  	 break;
	  	 }
	  	 return(dest);
	},
	 
	PREP_PHOTO : function(photoID, pageID, rotation){ 
		var canvas = document.createElement('canvas'),
			context = canvas.getContext('2d'),
			imageObj = new Image(), 
			orientation = undefined, 
			sameImage = false, 
			blob_store, 
			photoScale; 
		
		canvas.width = 560;
		canvas.height = 560;
		
		clearCanvas(canvas, context);
		
		// CALL IMAGE TO BE DRAWN
		if(SCRAPBOOK["PHOTOS"][photoID] === undefined){ 
			imageObj = ORIGINAL_PHOTO["GALLERY"][photoID]["imgObj"];
			photoScale = scalePhoto(imageObj);// SCALE IMAGE 
			
			imageObj.width = photoScale.width; 
			imageObj.height = photoScale.height;
			
		} else { 
			if(ORIGINAL_PHOTO["GALLERY"][photoID]["imgObj"]["src"] === SCRAPBOOK["PHOTOS"][photoID]["imgObj"]["src"]){ 
				imageObj = SCRAPBOOK["PHOTOS"][photoID]["imgObj"];
				sameImage = true;		
				
				photoScale = { 
					width : imageObj.width, 
					height: imageObj.height
				};
				
			} else { 
				imageObj = ORIGINAL_PHOTO["GALLERY"][photoID]["imgObj"];
				photoScale = scalePhoto(imageObj);// SCALE IMAGE 

				imageObj.width = photoScale.width; 
				imageObj.height = photoScale.height;
			}
		}
		
		// DEFINE CENTER OF IMAGE
		var x = canvas.width/2 - photoScale.width/2,
			y = canvas.height/2 - photoScale.height/2;
		
		// ROTATE IMAGE IF CAMERA DATA	
/*

		EXIF.getData(imageObj, function() {
			orientation = EXIF.getTag(this, "Orientation");
			
			scene = EXIF.getTag(this, "SceneType");
			sceneDesc = EXIF.getTag(this, "SceneCaptureType");
				
			// 1 PORTRAIT - RIGHTSIDE UP
			// 3 PORTRAIT - RIGHTSIDE DOWN 
			// 6 LANDSCAPE - RIGHTSIDE UP
			// 8 LANDSCAPE - RIGHTSIDE DOWN
			
		    // ORIENTATIONS 
		    if(orientation == 1){ 
			   // DO NOTHING 
			   
		    } else if(orientation == 3){ 
			   	context.translate(canvas.width,canvas.height); 
			   	context.rotate(Math.PI); 
			  
			   	
		    } else if(orientation == 6){
		    	context.rotate(0.5 * Math.PI); 
				context.translate(0,-canvas.height);
				
				
		    } else if(orientation == 8){
		    	context.rotate(-0.5 * Math.PI); 
				context.translate(-canvas.width,0);
			
		    }
		 
		});
*/

	   var degree = 0;  
	   
	   if(rotation !== undefined){	
	   		degree = SCRAPBOOK["PHOTOS"][photoID]["rotation"];
			if(rotation == "clockwise"){
			    if(degree == 360){ 
				    degree = 0;
			    }
			    
			    switch (degree){
				    //
				    case 0: 
					    context.translate(canvas.width, canvas.width / canvas.height);
					    context.rotate(Math.PI / 2);				    
				    break; 
				    case 90: 
					    context.translate(canvas.width, canvas.height);
					    context.rotate(Math.PI);				    
				    break; 
				    case 180: 
					    context.translate(0, 560);
					    context.rotate(Math.PI * 1.5);				    
				    break; 
				    case 270: 
						// do nothing
				    break; 
				    
			    } 
				
			    degree += 90;
				
			}
		}
		
		context.drawImage(imageObj,x,y,photoScale.width,photoScale.height);
context.restore();
		// STORE CANVAS DATA 
		blob_store = canvas.toDataURL("image/jpeg", 8.0);
		
		if(SCRAPBOOK["PHOTOS"][photoID] === undefined){ 
			SCRAPBOOK["PHOTOS"][photoID] = new SCRAPBOOK.PHOTO(photoID, pageID, ORIGINAL_PHOTO["GALLERY"][photoID]["fileName"],  x ,y, photoScale.width, photoScale.height, blob_store, imageObj, degree);
		} else { 
			SCRAPBOOK["PHOTOS"][photoID] = { 
				id : photoID,
				pageID : pageID, 
				fileName : "updated_"+ORIGINAL_PHOTO["GALLERY"][photoID]["fileName"],
				width : photoScale.width,
				height : photoScale.height,
				src : blob_store,
				x : x,
				y : y,
				imgObj : imageObj, 
				rotation : degree
			};

		}

	    // DRAW PREVIEW OF THE IMAGE IN UPLOAD SCREEN
	    document.getElementById("preview-photo").setAttribute("src", blob_store); 
		
		// Free resources
		canvas.remove();
			    
		SCRAPBOOK.LOAD_PAGE(pageID);
		
		// SCALE UPLOADED IMAGE UP OR DOWN 
		function scalePhoto(imgObj){ 
			var w = imgObj.width,
				h = imgObj.height, 
				targetWidth = 620, 
				targetHeight = 620,
				scale = { 
					width : null, 
					height : null
				},
				ratio = null;
		
			if(w > h){ // Landscape
				ratio = targetHeight/h;
				scale.width = w*ratio;
				scale.height = 620; 
		
			} else { // Portrait
				ratio = targetWidth/w;	
				scale.width = 620; 
				scale.height = h*ratio;
			
			}
			
			return (scale);
		}
		
	}, 

	PRINTOUT : function(){
	  	 var printout = document.createElement("img"),
	  	     canvas = document.createElement("canvas"), 
	  	     context = canvas.getContext('2d'), 
	  	     printoutReady = false, 
	  	     doc;
	  		 
	  		 canvas.width = 3300; 
	  		 canvas.height = 2550;
	  		 
	  		 context.fillStyle = "#ffffff";
	  		 context.fillRect(0, 0, canvas.width, canvas.height);
	  		
	  	 // Loop through every page 
	  	 for( page in SCRAPBOOK["PAGES"]){ 
	  	 	 pageObj = SCRAPBOOK["PAGES"][page];
		  	 dest = SCRAPBOOK.PRINT_LAYOUT(pageObj.id); 
		  	 pageImgObj = pageObj.img;
	
		  	 if(
		  	 	pageObj.id == 3 ||
		  	 	pageObj.id == 4 ||
		  	 	pageObj.id == 5 ||
		  	 	pageObj.id == 6 
		  	 
		  	 ){ 
			  	context.translate(canvas.width, (pageObj.imgObj.height+15));
				context.rotate(Math.PI); 
		  	 }
		  	 
		  	 // Draw page in place amongst all other pages on canvas
		  	 context.drawImage(pageObj.imgObj, dest.x, dest.y, dest.width, dest.height);
		  	 
		  	 // Canvas Reset
			 context.setTransform(1, 0, 0, 1, 0, 0);
			 
	  	 }
	  	 
	  	 // Turn canvas into source data
	  	 canvasData = canvas.toDataURL("image/jpeg", 0.5);
	  	 printout.src = canvasData; // load canvas data as an image
	  	 printout.setAttribute("id", "scrapbook_printout");
	
  	 	 // Convert image data to a file
		 var blobBin = atob(canvasData.split(',')[1]);
		 var buffer = new ArrayBuffer(blobBin.length);
		 var ba = new Uint8Array(buffer);
		 for (var i = 0; i < blobBin.length; i++) {
		     ba[i] = blobBin.charCodeAt(i);
		 }
		 
		 SCRAPBOOK.DOWNLOAD = new Blob([ba],{type:"image/jpeg"});
		 SCRAPBOOK.DOWNLOAD.filename = "My Trolls Scrapbook"; 
		 
	  	 printout.onload = function(){ 

	  	 	// Put Scrapbook Image Obj in 
		  	document.getElementById("printout_image").innerHTML = "";
		  	var a = document.createElement('a');
		  	a.appendChild(printout);
		  	a.setAttribute("id", "final_scrapbook_jpeg");
		  	a.title = "Trolls scrapbook printout";
		  	a.target = "_blank"; 
		  	if(document.documentElement.clientWidth > 1333){ a.href = printout.src; a.download = SCRAPBOOK.DOWNLOAD.filename; }
		  	document.getElementById("printout_image").appendChild(a);
			
		  	try {
		  		// ROTATE PRINTOUT TO FIT ON 8.5x11 PAPER
			  	var pdfCanvas = document.createElement('canvas');
			  		pdfCanvas.width = 3300;
			  		pdfCanvas.height = 2550; 
			  	 
			  	var pdfContext = pdfCanvas.getContext('2d');
		  		    pdfContext.fillStyle ="#ffffff";
		  		    pdfContext.fillRect(0,0,3300,3300);
		  		    pdfContext.translate(3300,0);
					pdfContext.rotate(0.5 * Math.PI); 
					
			  	pdfContext.drawImage(printout, 60, 78, 2430, 3144);
			  	
				/* POSTER IMAGE
				----------------------------------------- */
				SCRAPBOOK.PDF.addImage(pdfCanvas, 'JPEG', 0, 0, 8.5, 11);
				
				
				// Free resources
				canvas.remove();
		 
				/* INSTRUCTIONS PAGE 
				----------------------------------------- */
				var instructionsPage = document.getElementById("instructions-page"),
					instructionsCanvas = document.createElement("canvas"), 
					instructionsContext = instructionsCanvas.getContext('2d'); 
					
					instructionsCanvas.width = 850; 
					instructionsCanvas.height = 1100;
					
				instructionsContext.drawImage(instructionsPage, 0, 0, 850, 1100);
				
				var instructionsPageData = instructionsCanvas.toDataURL("image/jpeg", 1.0);
				
				SCRAPBOOK.PDF.addPage();
				SCRAPBOOK.PDF.addImage(instructionsPageData, 'JPEG', 0, 0, 8.5, 11);
			
			} catch(e) {
			
				alert("We're sorry, we had trouble building your Scrapbook.");
				$("#final-step-loading").hide();
				$("#final-step-pre").show();
			
			}

		 	printoutReady = true;
	  	 }
	  	 
	  	 printout.onerror = function(){
		  	  printoutReady = false;
	  	 }

	  	 return(printoutReady);
	  	 
	  	 // Free resources
		 instructionsCanvas.remove();


	 }, 
	 
	 PRINT_LAYOUT : function(pageID){ 
	  	 var dest = {};
	  	 
	  	 switch(pageID){ 
		  	 case 0: 
		  	 	dest.x = 830;
		  	 	dest.y = 1275; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;
		  	 case 1: 
		  	 	dest.x = 1650;
		  	 	dest.y = 1275; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;	
		  	 case 2: 
		  	 	dest.x = 2470;
		  	 	dest.y = 1275; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;	
		  	 case 3: 
		  	 	dest.x = 2470;
		  	 	dest.y = 0; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;	
		  	 case 4: 
		  	 	dest.x = 1650;
		  	 	dest.y = 0; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;	
		  	 case 5: 
		  	 	dest.x = 830;
		  	 	dest.y = 0; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;	
		  	 case 6: 
		  	 	dest.x = 10;
		  	 	dest.y = 0; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;	
		  	 case 7: 
		  	 	dest.x = 10;
		  	 	dest.y = 1275; 
		  	 	dest.width = 820; 
		  	 	dest.height = 1259;
		  	 break;		  	 		  	 
	  	 }
	  	 return dest;
	  }
};

// CANVAS UTILITIES
function clearCanvas(canvas, context){ 
	context.clearRect(0, 0, canvas.width, canvas.height);
} 