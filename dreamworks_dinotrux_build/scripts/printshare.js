var doc,
  poster_image_landscape,
  poster_image_landscape_lores,
  poster_image_portrait,

  pdfready = false,
  awsready = false,

  w = 3300,
  h = 2550,

  lores_w = 1398,
  lores_h = 1080,

  asset_format = "png",
  asset_quality = "",

  reqCount = 0,

  posterLayers = [],

  posterBackground,
  posterTail,
  posterTailCondition,
  posterRearLeg,
  posterRearLegCondition,
  posterBody,
  posterBodyCondition,
  posterFrontLeg,
  posterFrontLegCondition,
  posterHead,
  posterHeadCondition,
  posterFriend,
  posterProp,

  pieces = [], 
  conditions = [], 
  
  bodyPiece1,
  bodyCondition1,
  bodyPiece2,
  bodyCondition2,
  legPiece,
  legCondition,
  headPiece,
  headCondition,
  tailPiece,
  tailCondition,
  friendPropPiece,
  instructions;

  var desc = escape('My Dinotrux');

  var APIImage = {
    config: {
      verifyUploadURL: '/shares/verify_upload?token=',
      sharingURL: '/upload/share_image',
      description : desc,
      activityKey : 'dinotrux',
    }
  };

  if(window.location.protocol == 'https:') {
     APIImage.config.protocol = 'https:';
  }else{
     APIImage.config.protocol = 'http:';
  }

  if(window.location.hostname === '0.0.0.0' || window.location.hostname === 'localhost') {
     APIImage.config.endPointURL = '';
  }else{
     APIImage.config.endPointURL =  APIImage.config.protocol + '//' + window.location.hostname;
  }

//iOS hack fix
try {
  if(
    /iPad|iPhone|iPod/i.test(navigator.userAgent) && 
    !window.MSStream 

    // &&
    // !/os 9_/i.test(navigator.userAgent) && 
    // !/os 10_/i.test(navigator.userAgent) && 
    // !/os 11_/i.test(navigator.userAgent) && 
    // !/os 12_/i.test(navigator.userAgent)

  ) {
    w = 2264;
    h = 1750;
    asset_quality = '-lores';
  }
} catch(e) {}


function checkLoadedAndGenerateAssets() {

  reqCount--;
  if(reqCount == 0) {

    //////////////////////////////////////////////
    //
    // BUILD LANDSCAPE IMAGE
    //
    //////////////////////////////////////////////

      var fontsize = parseInt(w / 17.18);

      var landscape_canvas = document.createElement('canvas');
      landscape_canvas.width = w;
      landscape_canvas.height = h;

      var landscape_context = landscape_canvas.getContext('2d');
      landscape_context.fillStyle = "white";
      landscape_context.fillRect(0, 0, w, h);
      
      for(var i = 0; i < posterLayers.length; i++) {
        //make sure it's not an image that errored
        if(posterLayers[i].width == 0) continue;

        if(i == 0) landscape_context.drawImage(posterLayers[i], 0, 0, w, h);
        else {
          landscape_context.drawImage(posterLayers[i], 0.3190 * w, 0.26 * h, 0.4054 * w, 0.5784 * h);
        }
      }

      landscape_context.font = fontsize + "px MuseoSansW01-Rounded900";
      landscape_context.textAlign = "center";
      landscape_context.fillText(character.name1 + character.name2 + character.name3, 0.51 * w,  0.265 * h);

      poster_image_landscape = landscape_canvas.toDataURL('image/jpeg', .90);
      //landscape_canvas.remove();

    //////////////////////////////////////////////
    //
    // BUILD LANDSCAPE IMAGE (LORES)
    //
    //////////////////////////////////////////////

      var fontsize_lores = parseInt(lores_w / 17.18);

      var landscape_lores_canvas = document.createElement('canvas');
      landscape_lores_canvas.width = lores_w;
      landscape_lores_canvas.height = lores_h;

      var landscape_lores_context = landscape_lores_canvas.getContext('2d');
      landscape_lores_context.fillStyle = "white";
      landscape_lores_context.fillRect(0, 0, lores_w, lores_h);
      
      for(var i = 0; i < posterLayers.length; i++) {
        //make sure it's not an image that errored
        if(posterLayers[i].width == 0) continue;

        if(i == 0) landscape_lores_context.drawImage(posterLayers[i], 0, 0, lores_w, lores_h);
        else {
          landscape_lores_context.drawImage(posterLayers[i], 0.3190 * lores_w, 0.26 * lores_h, 0.4054 * lores_w, 0.5784 * lores_h);
        }
      }

      landscape_lores_context.font = fontsize_lores + "px MuseoSansW01-Rounded900";
      landscape_lores_context.textAlign = "center";
      landscape_lores_context.fillText(character.name1 + character.name2 + character.name3, 0.51 * lores_w,  0.265 * lores_h);

      poster_image_landscape_lores = landscape_lores_canvas.toDataURL('image/jpeg', .90);
      //landscape_lores_canvas.remove();

    //////////////////////////////////////////////
    //
    // BUILD PORTRAIT IMAGE
    //
    //////////////////////////////////////////////

      var portrait_canvas = document.createElement('canvas');
      portrait_canvas.width = h;
      portrait_canvas.height = w;

      var portrait_context = portrait_canvas.getContext('2d');
      portrait_context.fillStyle = "white";
      portrait_context.fillRect(0, 0, h, w);
      portrait_context.translate(0, w);
      portrait_context.rotate(-Math.PI / 2);
      
      for(var i = 0; i < posterLayers.length; i++) {
        //make sure it's not an image that errored
        if(posterLayers[i].width == 0) continue;

        if(i == 0) portrait_context.drawImage(posterLayers[i], 0, 0, w, h);
        else {
          portrait_context.drawImage(posterLayers[i], 0.3190 * w, 0.26 * h, 0.4054 * w, 0.5784 * h);
        }
      }

      portrait_context.font = fontsize + "px MuseoSansW01-Rounded900";
      portrait_context.textAlign = "center";
      portrait_context.fillText(character.name1 + character.name2 + character.name3, 0.51 * w,  0.265 * h);

      poster_image_portrait = portrait_canvas.toDataURL('image/jpeg', .90);
      //portrait_canvas.remove();

    //////////////////////////////////////////////
    //
    // UPLOAD TO SHARE SERVER
    //
    //////////////////////////////////////////////

      // set the image title
      //APIImage.config.title = escape(character.name1 + character.name2 + character.name3);

      // create property
      //APIImage.config.filename = character.name1.replace(' ', '').toLowerCase() + '-' + character.name2.replace(' ', '').toLowerCase() + '-' + character.name3.replace(' ', '').toLowerCase() +'-' + generateUUID() +'.jpg';

      // split up data URL
      //var urlSplit = poster_image_landscape_lores.split(',');
      //var dataType = urlSplit[0];
      //var base64Data = urlSplit[1];
      //var mimeType = dataType.match(/:([^;]*)/)[1];  // "image/png"

      // var fd = new FormData(), xhr;

      // fd.append("title", APIImage.config.title);  
      // fd.append("description", APIImage.config.description);
      // fd.append("activity-key", APIImage.config.activityKey);
      // fd.append("image", base64Data);
      // fd.append("mime-type", mimeType);

      // xhr = new XMLHttpRequest();

      // xhr.onreadystatechange = function() {

      //   if (xhr.readyState == 4) {

      //     var resp = JSON.parse(xhr.responseText);

      //     //console.log('resp.response',resp.response);

      //     // create property
      //     APIImage.config.imageURL = APIImage.config.endPointURL +  APIImage.config.verifyUploadURL  + resp.response.token;
          
      //     APIImage.config.viewURL = resp.response.url;

      //     awsready = true;
      //     updateFinalStepUI();

      //   }else{

      //     // Will fail at first as it waits fro about 5 seconds for the reply from server
      //     //console.log("API FAIL");
          
      //   }

      // }

      // xhr.open("POST", APIImage.config.endPointURL + APIImage.config.sharingURL);
      // xhr.send(fd);

    //////////////////////////////////////////////
    //
    // CREATE PDF
    //
    //////////////////////////////////////////////

      doc = new jsPDF('p', 'in', [8.5, 11]);
      
      setTimeout( function(){
        // Time out set for 3.5 seconds to allow building animation to play
        
        try {
          //1st page
          doc.addImage(poster_image_portrait, 'JPEG', 0, 0, 8.5, 11);

          //all other pages
          for(var i = 0; i < pieces.length; i++) {

            //make sure it's not an image that errored
            if(pieces[i].width == 0) continue;

            doc.addPage();
            
            var canvas = document.createElement('canvas');
            canvas.width = w;
            canvas.height = h;

            var context = canvas.getContext('2d');
            context.fillStyle = "white";
            context.fillRect(0, 0, w, h);
            context.drawImage(pieces[i], 0, 0, w, h);

            if(typeof conditions[i] !== 'undefined' && conditions[i].width != 0) {
              context.drawImage(conditions[i], 0, 0, w, h);
            }

            var imgdata = canvas.toDataURL('image/jpeg', .90);
            doc.addImage(imgdata, 'JPEG', 0, 0, 8.5, 11);
            //canvas.remove();

          }

          // debug ////////////////////////////////////////

          /*doc.addPage();

          var canvas = document.createElement('canvas');
          canvas.width = w;
          canvas.height = h;

          var context = canvas.getContext('2d');
          context.fillStyle = "white";
          context.fillRect(0, 0, w, h);
          
          context.font = "72px monaco, consolas, monospace";
          context.fillStyle = "black";

          var counter = 0;
          for(var i in character) {
            counter++;
            context.fillText(i + ": " + character[i], 100, 100 * counter);  
          }

          var imgdata = canvas.toDataURL('image/jpeg', .90);
          doc.addImage(imgdata, 'JPEG', 0, 0, 8.5, 11);*/

          // debug ////////////////////////////////////////

          pdfready = true;
          updateFinalStepUI();

        }

        catch(e) {
          alert("We're sorry, we had trouble building your Dinotrux.");
          $("#final-step-loading").hide();
          $("#final-step-pre").show();
          console.log(e);
        }

    }, 3500);

  }

}

function updateFinalStepUI() {

  if(pdfready) {

    // creates href to /verify_upload/ 
    //$("#final-step-share").attr('href', APIImage.config.imageURL);

    $("#download .image").append('<img src="' + poster_image_landscape + '" />');
    //$("#download .image").append('<img src="' + APIImage.config.viewURL + '" />');
    $("#final-step-download").on("click", function(){ $("#shade, #download").toggleClass("open"); });

    $("#final-step-loading").hide();
    $("#final-step-post").show();

    $(".ty-copy, .ty_rivet_download").fadeIn('fast');
    trackActivityAction('dinotrux_builder','activity_complete','activity_complete');
    
  }
}

$(document).ready(function(){

  $("#final-step-generate-assets").on("click", kickoffAssetGeneration);

  $("#final-step-print").on("click", function(){
    if(doc) {
      var fn = character.name1.replace(' ', '').toLowerCase() + '-' + character.name2.replace(' ', '').toLowerCase() + '-' + character.name3.replace(' ', '').toLowerCase() +'.pdf';
      doc.save(fn);
    }
  });

});

function kickoffAssetGeneration() {

  //this dispatch function helps alliviate the noticiability of animation freezes
  //during asset/pdf generation

  //functionality it doesn't do any except to make sure the animation is visible and running before
  //doing the heavy lifting

  //since the animation is a step/discrete function it's less noticiable than
  //a smooth moving progress bar or spinner

  $("#final-step-pre").hide();
  $("#final-step-loading").show();

  setTimeout(preloadAssetImages, 100);
  
}

function assetFailed() {
  //simply try again here, and then give up

  var img = new Image();
  img.onload = this.onload;

  //be careful uncommenting this line- it will repeatedly try to fetch failed images
  //this will be an infinite loop for most errors (404 are the most obvious)
  //img.onerror = this.onerror;

  img.onerror = function() {
    //console.log(this);

    //uncomment this section to prevent PDFs from generating with missing assets

    //alert("We're sorry, we had trouble creating your Dinotrux.");
    //$("#final-step-loading").hide();
    //$("#final-step-pre").show();

    //recomment this line if you uncomment the section above
    checkLoadedAndGenerateAssets();
  }

  img.src = this.src;
  pieces.push(img);
}

function preloadAssetImages() {

  doc = null,
  poster_image_landscape = null,
  poster_image_landscape_lores = null,
  poster_image_portrait = null,

  pdfready = false,
  awsready = false,
  
  reqCount = 0,

  posterLayers = [],

  posterBackground = new Image(),
  posterTail = new Image(),
  posterTailCondition = new Image(),
  posterRearLeg = new Image(),
  posterRearLegCondition = new Image(),
  posterBody = new Image(),
  posterBodyCondition = new Image(),
  posterFrontLeg = new Image(),
  posterFrontLegCondition = new Image(),
  posterHead = new Image(),
  posterHeadCondition = new Image(),
  posterFriend = new Image(),
  posterProp = new Image(),

  pieces = [], 
  conditions = [], 
  
  bodyPiece1 = new Image(),
  bodyCondition1 = new Image(),
  bodyPiece2 = new Image(),
  bodyCondition2 = new Image(),
  legPiece = new Image(),
  legCondition = new Image(),
  headPiece = new Image(),
  headCondition = new Image(),
  tailPiece = new Image(),
  tailCondition = new Image(),
  friendPropPiece = new Image(),
  instructions = new Image();

  posterBackground.onload = checkLoadedAndGenerateAssets;
  posterBackground.onerror = assetFailed;
  posterBackground.src = 'assets/dinotrux/jpg' + asset_quality + '/poster-bg-' + character.background + '.jpg';
  posterLayers.push(posterBackground);
  reqCount++;

  posterTail.onload = checkLoadedAndGenerateAssets;
  posterTail.onerror = assetFailed;
  posterTail.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/color-' + character.color + '/tail-' + character.tail + '.' + asset_format;
  posterLayers.push(posterTail);
  reqCount++;

  if(character.condition > 0) {
    posterTailCondition.onload = checkLoadedAndGenerateAssets;
    posterTailCondition.onerror = assetFailed;
    posterTailCondition.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/condition-' + character.condition + '/tail-' + character.tail + '.' + asset_format;
    posterLayers.push(posterTailCondition);
    reqCount++;
  }

  posterRearLeg.onload = checkLoadedAndGenerateAssets;
  posterRearLeg.onerror = assetFailed;
  posterRearLeg.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/color-' + character.color + '/leg-rear-' + character.feet + '.' + asset_format;
  posterLayers.push(posterRearLeg);
  reqCount++;

  if(character.condition > 0) {
    posterRearLegCondition.onload = checkLoadedAndGenerateAssets;
    posterRearLegCondition.onerror = assetFailed;
    posterRearLegCondition.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/condition-' + character.condition + '/leg-rear-' + character.feet + '.' + asset_format;
    posterLayers.push(posterRearLegCondition);
    reqCount++;
  }

  posterBody.onload = checkLoadedAndGenerateAssets;
  posterBody.onerror = assetFailed;
  posterBody.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/color-' + character.color + '.' + asset_format;
  posterLayers.push(posterBody);
  reqCount++;

  if(character.condition > 0) {
    posterBodyCondition.onload = checkLoadedAndGenerateAssets;
    posterBodyCondition.onerror = assetFailed;
    posterBodyCondition.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/condition-' + character.condition + '/body-' + character.body + '.' + asset_format;
    posterLayers.push(posterBodyCondition);
    reqCount++;
  }

  posterFrontLeg.onload = checkLoadedAndGenerateAssets;
  posterFrontLeg.onerror = assetFailed;
  posterFrontLeg.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/color-' + character.color + '/leg-front-' + character.feet + '.' + asset_format;
  posterLayers.push(posterFrontLeg);
  reqCount++;

  if(character.condition > 0) {
    posterFrontLegCondition.onload = checkLoadedAndGenerateAssets;
    posterFrontLegCondition.onerror = assetFailed;
    posterFrontLegCondition.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/condition-' + character.condition + '/leg-front-' + character.feet + '.' + asset_format;
    posterLayers.push(posterFrontLegCondition);
    reqCount++;
  }

  if(character.friend) {
    posterFriend.onload = checkLoadedAndGenerateAssets;
    posterFriend.onerror = assetFailed;
    posterFriend.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/friend-' + character.friend + '.' + asset_format;
    posterLayers.push(posterFriend);
    reqCount++;
  }

  posterHead.onload = checkLoadedAndGenerateAssets;
  posterHead.onerror = assetFailed;
  posterHead.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/color-' + character.color + '/head-' + (character.expression  + (4 * (character.species - 1))) + '.' + asset_format;
  posterLayers.push(posterHead);
  reqCount++;

  if(character.condition > 0) {
    posterHeadCondition.onload = checkLoadedAndGenerateAssets;
    posterHeadCondition.onerror = assetFailed;
    posterHeadCondition.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/body-' + character.body + '/condition-' + character.condition + '/head-' + character.species + '.' + asset_format;
    posterLayers.push(posterHeadCondition);
    reqCount++;
  }

  if(character.prop) {
    posterProp.onload = checkLoadedAndGenerateAssets;
    posterProp.onerror = assetFailed;
    posterProp.src = 'assets/dinotrux/' + asset_format + asset_quality + '/web/prop-' + character.prop + '.' + asset_format;
    posterLayers.push(posterProp);
    reqCount++;
  }

  bodyPiece1.onload = checkLoadedAndGenerateAssets;
  bodyPiece1.onerror = assetFailed;
  bodyPiece1.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/color-' + character.color + '/body-' + character.body + '-1.png';
  pieces.push(bodyPiece1);
  reqCount++;

  if(character.condition > 0)  {
    bodyCondition1.onload = checkLoadedAndGenerateAssets;
    bodyCondition1.onerror = assetFailed;
    bodyCondition1.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/condition-' + character.condition + '/body-' + character.body + '-1.png';
    conditions.push(bodyCondition1);
    reqCount++;
  }
  
  if(character.body == 2 || character.body == 3 || character.body == 5) {
    bodyPiece2.onload = checkLoadedAndGenerateAssets;
    bodyPiece2.onerror = assetFailed;
    bodyPiece2.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/color-' + character.color + '/body-' + character.body + '-2.png';
    pieces.push(bodyPiece2);
    reqCount++;

    if(character.condition > 0) {
      bodyCondition2.onload = checkLoadedAndGenerateAssets;
      bodyCondition2.onerror = assetFailed;
      bodyCondition2.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/condition-' + character.condition + '/body-' + character.body + '-2.png';
      conditions.push(bodyCondition2);
      reqCount++;
    }
  }
  
  legPiece.onload = checkLoadedAndGenerateAssets;
  legPiece.onerror = assetFailed;
  legPiece.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/color-' + character.color + '/leg-' + character.feet + '.png';
  pieces.push(legPiece);
  reqCount++;

  if(character.condition > 0)  {
    legCondition.onload = checkLoadedAndGenerateAssets;
    legCondition.onerror = assetFailed;
    legCondition.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/condition-' + character.condition + '/leg-' + character.feet + '.png';
    conditions.push(legCondition);
    reqCount++;
  }

  headPiece.onload = checkLoadedAndGenerateAssets;
  headPiece.onerror = assetFailed;
  headPiece.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/color-' + character.color + '/head-' + character.species + '-' + character.expression + '.png';
  pieces.push(headPiece);
  reqCount++;

  if(character.condition > 0) {
    headCondition.onload = checkLoadedAndGenerateAssets;
    headCondition.onerror = assetFailed;
    headCondition.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/condition-' + character.condition + '/head-' + character.species + '-' + character.expression  + '.png';
    conditions.push(headCondition);
    reqCount++;
  }

  tailPiece.onload = checkLoadedAndGenerateAssets;
  tailPiece.onerror = assetFailed;
  tailPiece.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/color-' + character.color + '/tail-' + character.tail + '.png';
  pieces.push(tailPiece);
  reqCount++;

  if(character.condition > 0) {
    tailCondition.onload = checkLoadedAndGenerateAssets;
    tailCondition.onerror = assetFailed;
    tailCondition.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/condition-' + character.condition + '/tail-' + character.tail + '.png';
    conditions.push(tailCondition);
    reqCount++;
  }

  if(character.friend || character.prop) {
    friendPropPiece.onload = checkLoadedAndGenerateAssets;
    friendPropPiece.onerror = assetFailed;
    friendPropPiece.src = 'assets/dinotrux/png' + asset_quality + '/papercraft/prop-' + character.prop + '_friend-' + character.friend + '.png';
    pieces.push(friendPropPiece);
    reqCount++;
  }

  instructions.onload = checkLoadedAndGenerateAssets;
  instructions.onerror = assetFailed;
  instructions.src = 'assets/imgs/instructions' + asset_quality + '.jpg';
  pieces.push(instructions);
  reqCount++;

}

/////////////////////////////////////////
// Utility Functions Active
/////////////////////////////////////////

function generateUUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random()*16)%16 | 0;
    d = Math.floor(d/16);
    return (c=='x' ? r : (r&0x3|0x8)).toString(16);
  });
  return uuid;
}