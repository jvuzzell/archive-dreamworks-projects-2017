# Dreamworks Create Dinotrux Activity Source/Build #

Please see CHANGELOG.md for updates

This repository is for the Create Dinotrux activity

Deployment is now handled out of a separate repository located at https://github.com/dreamworksanimation/create_activity_deployment
