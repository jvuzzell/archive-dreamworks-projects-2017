var RESTART = false,
    jumpToSlide = false,
	sfx_music, sfx_scream, sfx_click, mute = false, firstplay = true;

$(document).ready(function(){
	SLIDE.INIT();
	
	FastClick.attach(document.body);
	
	/* SET STAGE DIMENSIONS
	---------------------------------- */
	bindWindowSizeEvent(function(){
		$("#stage").width("100%");
		$("#stage").height((9 * parseInt($("#stage").width())) / 16);
	});
	
	/* STEPPING THROUGH SLIDES
	---------------------------- */
	SLIDE.LENGTH();

	$startButton = $("#start-button");
	$stepForward = $("#step-forward"); 
	$stepBackward = $("#step-backward"); 

	$startButton.on("click", function(){
		leaveCurrentSlide();
		setSlideTransitionDelay("next");
	});

	$("#step-forward > a.hit-box").on("click", function(){
		leaveCurrentSlide();
		setSlideTransitionDelay("next");
	});

	$("#step-backward > a.hit-box").on("click", function(){
		leaveCurrentSlide();
		setSlideTransitionDelay("previous");
	});

	$("#step-forward > a.hit-box, #step-backward > a.hit-box").hover(function(){
		$(this).parent().addClass('hover');
	}, function(){
		$(this).parent().removeClass('hover');
	});
	/*---------------------------*/
	
	$(window).on("blur", pauseAudio);
	$(window).on("focus", playAudio);
	
	/* PAGE SELECT
	--------------------------------- */
	$(".photo-options[data-page]").on("click", function(){
		PAGE.SELECT(SLIDE.ACTIVE, "page-click", $(this).data("page"));
	});
    
	
	/* BUILD SCRAPBOOK VIEWS AFTER PAGES VIEWS and PAGE CONTROLS HAVE BEEN BUILT
	------------------------------------------------------------------------------- */
	SCRAPBOOK.INIT();


	/* THEME SELECT
    --------------------------------- */
	$(".theme-option[data-themeid='1']").addClass("active");
	$(".theme-option[data-titleid='1']").addClass("active");  
    
    $(".theme-option").on("click",function(){ 
       if("a.theme-option.active div.theme-hover.flash"){ 
	       $("a.theme-option.active div.theme-hover").removeClass("flash");
       }
       
	   var pageNum = parseInt($(this).attr("data-page")),
	   	   themeID = parseInt($(this).attr("data-themeid")),
	   	   assetType = "theme";
	   	   $lastActive = $(".theme-option[data-page='"+pageNum+"'].active");
	   	   
	   
	   if(pageNum === 0){
			if($(this).attr("data-titleid") === undefined){
				$(".slide[data-slide='2'] .theme-option").removeClass("active");
				
			} else {
			    $(".slide[data-slide='3'] .theme-option").removeClass("active");
			    themeID = parseInt($(this).attr("data-titleid"));
			    assetType = "title";
			}
	   } else {
		   $lastActive.removeClass("active");
		   
	   }
	   
	   $(this).addClass("active"); 
	   
	   SCRAPBOOK.SET_THEME(pageNum, themeID, assetType); 
	   SCRAPBOOK["PAGES"]["page_"+pageNum]["registered"] = false;
    });
	
	
	/* PHOTO INPUT HOVER
	--------------------------------- */
    $(".inputs a.hover-effect").hover(
    	function(){
    		var num = $(this).data("photo");
        	$("div.photo_"+num).css("background-image","url('img/photo-icon-hover.png')");
    	}, 
    	function(){
        	var num = $(this).data("photo");
        	$("div.photo_"+num).css("background-image","url('img/photo-icon.png')");
    	}
    );
	
	$(".fileToUpload").hover(
		function(){ 
			$("#preview-photo-container .overlay").css("background-image","url('img/upload-screen/photo-icon-hover.jpg')");
		},
		function(){ 
			$("#preview-photo-container .overlay").css("background-image","url('img/upload-screen/photo-icon.jpg')");
		}
	);
    // Alter input on 'TITLE' selection screen so that users can access the photo on the front cover
    $(".slide[data-slide='3'] a.photo_1").addClass("photo_0").removeClass("photo_1").attr("data-photo","0");

    
    /* PHOTO TO UPLOAD SELECT 
    -------------------------------- */
    $(".inputs a.hover-effect").on("click", function(){ 
	    $("#upload-modal, #upload-screen").toggleClass("open");
	    
	    var photoID = $(this).data("photo");
	    var $uploadWindow = $("#upload-window"); 
	    
		$("input.fileToUpload").attr("data-photo", photoID);
		$("input.fileToUpload").prop("value","");
		$("#upload-frame").attr("data-photo", photoID);
		$("#preview-photo").attr("data-photo", photoID);
		
		if(SCRAPBOOK["PHOTOS"]["photo_"+photoID] === undefined){
			$("#preview-photo").attr("src", "img/upload-screen/blank.png");
			$("#rotate-clockwise a").addClass("disabled");	
		} else { 
			$("#preview-photo").attr("src", SCRAPBOOK["PHOTOS"]["photo_"+photoID]["src"]);
			$("#rotate-clockwise a").removeClass("disabled");
		}
		
		$("input.fileToUpload").click();
    });

    /* PHOTO UPLOADING 
    ------------------------------ */
 	$(".fileToUpload").change(function (e) {
 		$("#rotate-clockwise a").removeClass("disabled");
 		SLIDE.INSTRUCTIONS.PIC_UPLOADED = true;
 		
		if(this.disabled) return alert('File upload not supported!'); 
		var F = this.files;
		var inputID = "photo_";
		inputID += e.target.getAttribute("data-photo");
		
		var pageID = parseInt($("#pages").attr("data-page"));
		
		// Supports multiple files uploaded at once time.  
		// However, currently setup to function with one image.
		if(F && F[0]) for(var i=0; i<F.length; i++){
			//
			ORIGINAL_PHOTO.UPLOAD(F[i], inputID, pageID);
			//
			if(SCRAPBOOK["PAGES"]["page_"+pageID]["photoIDs"] == inputID){
				SCRAPBOOK["PAGES"]["page_"+pageID]["photoIDs"].splice([inputID], 1);
			}
			SCRAPBOOK["PAGES"]["page_"+pageID]["photoIDs"].push(inputID);
		};
	});   

    /* PHOTO MANIPULATION 
    ------------------------------ */   
	$("#rotate-clockwise a").on("click",function(){rotateImg("clockwise")});
	$("#rotate-counter-clockwise a").on("click",function(){rotateImg("counter-clockwise")});
	
	var rotateImg = function (direction){ 
		var photoID = "photo_";
			photoID += $("input.fileToUpload").attr("data-photo");
		var pageNum = $(".slide-container").attr("data-page");
		SCRAPBOOK.PREP_PHOTO(photoID, pageNum, direction);
	}
	
    /* FINAL SCREEN FUNCTIONS 
    -------------------------------- */
    $("#final-step-download").on("click", function(){ $("#shade, #download").toggleClass("open"); $("#shade").addClass("download");});
    
    $("#return-to-start").on("click", function(){ $("#shade, #restart").toggleClass("open"); $("#shade").addClass("restart");});
    
	$("#download a.close").on("click", function(){ $("#shade, #download").toggleClass("open"); });
	
	$("#restart-yes, #restart-no, #restart a.close").on("click", function(){ $("#shade, #restart").toggleClass("open"); });
	
	$("#restart-yes").on("click", returnToStart);

	/* Close Upload Modal 
	--------------------- */
	window.onclick = function(event) {
	
		var uploadModal = document.getElementById('upload-modal'), 
			uploadPop = document.getElementById('upload-screen');
		
	    var popup, modal;
	    
	    if (event.target == uploadModal) {
	        uploadModal.className="";
	        uploadPop.className="popup";
	        
	        if(SLIDE.INSTRUCTIONS.FIRSTTIME == true && SLIDE.INSTRUCTIONS.PIC_UPLOADED == true){ 
		       $("#instructional-2").removeClass("hidden");
		       $("#instructional-1").addClass("hidden");
		       $("a.theme-option.active div.theme-hover").addClass("flash");
		       SLIDE.INSTRUCTIONS.FIRSTTIME = false; 
	        }	
		}
	}
	
	$("#upload-screen a.close").on("click", function(){ 
		$("#upload-screen, #upload-modal").toggleClass("open"); 
		if(SLIDE.INSTRUCTIONS.FIRSTTIME == true && SLIDE.INSTRUCTIONS.PIC_UPLOADED == true){ 
	       $("#instructional-2").removeClass("hidden");
	       $("#instructional-1").addClass("hidden");
	       $("a.theme-option.active div.theme-hover").addClass("flash");
	       SLIDE.INSTRUCTIONS.FIRSTTIME = false; 
        }	
	});
	
	/* ----------------------------- */
		
	/* PRINT & SHARE
	--------------------------------- */
	$("#step-forward a").on("click", function(){ 
		var printoutReady = false, 
			previewZone = document.getElementById("preview-scrapbook"); 
			
		if(SLIDE.ACTIVE == 7){ 
			printoutReady = SCRAPBOOK.PRINTOUT();
			previewZone.innerHTML = ""; 
			
			var exisitingPreview = document.getElementById("preview_cover");
			var clone = SCRAPBOOK.PAGES.page_0.imgObj.cloneNode(true); 
			
			// Free resource
			if(exisitingPreview !== null){ 
				exisitingPreview.remove();
			}
			
			clone.setAttribute("id", "preview_cover");
			previewZone.appendChild(clone);
		}		
	});
	
	$("#final-step-print").on("click", function(){
		if(SCRAPBOOK.PDF) {
		  var fn = 'My Trolls Scrapbook.pdf';
		  SCRAPBOOK.PDF.save(fn);
		}
	});
	
	

							
});

/* START APP!!!
----------------------------------- */

function startApp() {
	if(!LOADED) {
		
		loadingScreenExit();

		//enable audio
		$('#play').click(playAudio);
		$('#pause').click(function(){if(firstplay == true){firstplay = false; playAudio();}else{pauseAudio()}});
		$("#start-button").click(function(){
			firstplay = false;
			if(!mute) playAudio();
			// if(!mute) sfx_nav_click.play();
		});
		$('a.option, a, .forward, .backward, input').click(function(){
			if(!mute) sfx_opt_click.play();
		});
		$('#step-forward, #step-back').click(function(){
			if(!mute) sfx_nav_click.play();
		});
		$(".fileToUpload").on("change", function(){
			if(!mute) sfx_opt_click.play();
		});


		LOADED = true;
	}
}

function playAudio() {
	$('#play').css('display','none');
	$('#pause').css('display','block');
	mute = false;
	if(firstplay) return;
	
	sfx_music.play();
	sfx_music.loop = true;
}

function pauseAudio() {
	$('#pause').css('display','none');
	$('#play').css('display','block');
	mute = true;
	sfx_music.pause();
	sfx_nav_click.pause();
	sfx_nav_click.currentTime = 0;
	sfx_opt_click.pause();
	sfx_opt_click.currentTime = 0;
}

// Called in Preload.js
function audioEqualizer(){ 
	sfx_music.volume     = 1.0;
	sfx_opt_click.volume = 0.4;
	sfx_nav_click.volume = 0.35;
}

/////////////////////////////////////////
// UTILITY FUNCTIONS
/////////////////////////////////////////

/* WINDOW RESIZING FUNCTIONS
----------------------------------- */
function bindWindowSizeEvent(func)
{
	$(window).on("debouncedresize", func);  
	$(window).on("orientationchange", function(){ setTimeout(func, 150);});
	func();
}
function bindIntereactionEvent(func)
{
	$(window).on("debouncedresize scroll touchstart touchmove touchend", func);
	$(window).on("orientationchange", function(){ setTimeout(func, 150);});
	func();
}